package gogen

import (
	"reflect"

	"gitlab.com/creichlin/gogen/internal/fmtsort"
)

type iterateElement struct {
	key   interface{}
	value interface{}
}

func iteratorFor(val interface{}) ([]iterateElement, error) {
	ref := reflect.ValueOf(val)

	// if it's a pointer or interface, dereference it
	for ref.Kind() == reflect.Ptr || ref.Kind() == reflect.Interface {
		ref = ref.Elem()
	}

	len := ref.Len()
	elements := make([]iterateElement, 0, len)
	switch ref.Kind() {
	case reflect.Array, reflect.Slice, reflect.Chan:
		for i := 0; i < len; i++ {
			elements = append(elements, iterateElement{
				key:   i,
				value: ref.Index(i).Interface(),
			})
		}
	case reflect.String:
		for i, char := range ref.String() {
			elements = append(elements, iterateElement{
				key:   i,
				value: string(char),
			})
		}
	case reflect.Map:
		sorted := fmtsort.Sort(ref)
		for i, k := range sorted.Key {
			elements = append(elements, iterateElement{
				key:   k.Interface(),
				value: sorted.Value[i].Interface(),
			})
		}
	}

	return elements, nil
}
