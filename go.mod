module gitlab.com/creichlin/gogen

go 1.13

require (
	github.com/antlr/antlr4 v0.0.0-20200712162734-eb1adaa8a7a6
	github.com/davecgh/go-spew v1.1.1
	github.com/ghodss/yaml v1.0.0
	github.com/kr/pretty v0.2.1 // indirect
	github.com/pkg/errors v0.9.1
	gitlab.com/testle/expect v0.0.0-20200622171625-f00c39dcf2c4
	gopkg.in/yaml.v2 v2.3.0
)
