// Code generated from ./GoGenTemplate.g4 by ANTLR 4.8. DO NOT EDIT.

package ggt // GoGenTemplate
import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 46, 256,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4, 18, 9,
	18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 4, 23, 9, 23,
	4, 24, 9, 24, 4, 25, 9, 25, 4, 26, 9, 26, 4, 27, 9, 27, 4, 28, 9, 28, 4,
	29, 9, 29, 4, 30, 9, 30, 3, 2, 3, 2, 3, 3, 3, 3, 3, 4, 3, 4, 3, 5, 3, 5,
	3, 5, 3, 5, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 5, 6, 76, 10, 6, 3, 6, 3, 6,
	7, 6, 80, 10, 6, 12, 6, 14, 6, 83, 11, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 7,
	3, 7, 3, 7, 3, 7, 3, 7, 3, 8, 3, 8, 3, 8, 3, 8, 3, 9, 3, 9, 3, 9, 5, 9,
	101, 10, 9, 3, 9, 3, 9, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 11, 3, 11,
	3, 11, 3, 11, 5, 11, 114, 10, 11, 3, 11, 3, 11, 5, 11, 118, 10, 11, 3,
	11, 3, 11, 3, 11, 3, 11, 7, 11, 124, 10, 11, 12, 11, 14, 11, 127, 11, 11,
	5, 11, 129, 10, 11, 3, 11, 3, 11, 3, 12, 3, 12, 3, 12, 3, 12, 3, 13, 3,
	13, 3, 13, 7, 13, 140, 10, 13, 12, 13, 14, 13, 143, 11, 13, 5, 13, 145,
	10, 13, 3, 14, 3, 14, 3, 14, 3, 14, 3, 15, 3, 15, 3, 15, 3, 16, 3, 16,
	3, 16, 5, 16, 157, 10, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3,
	16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16,
	3, 16, 7, 16, 177, 10, 16, 12, 16, 14, 16, 180, 11, 16, 3, 17, 3, 17, 3,
	17, 3, 17, 3, 17, 3, 17, 5, 17, 188, 10, 17, 3, 18, 3, 18, 3, 18, 3, 19,
	3, 19, 3, 19, 3, 19, 5, 19, 197, 10, 19, 3, 20, 3, 20, 3, 21, 5, 21, 202,
	10, 21, 3, 21, 3, 21, 3, 22, 5, 22, 207, 10, 22, 3, 22, 3, 22, 3, 22, 3,
	22, 3, 23, 3, 23, 3, 24, 3, 24, 3, 24, 6, 24, 218, 10, 24, 13, 24, 14,
	24, 219, 3, 24, 6, 24, 223, 10, 24, 13, 24, 14, 24, 224, 5, 24, 227, 10,
	24, 3, 24, 5, 24, 230, 10, 24, 3, 25, 3, 25, 3, 25, 5, 25, 235, 10, 25,
	5, 25, 237, 10, 25, 3, 26, 3, 26, 3, 26, 5, 26, 242, 10, 26, 3, 27, 3,
	27, 3, 27, 3, 28, 3, 28, 3, 28, 3, 28, 3, 29, 3, 29, 3, 29, 3, 30, 3, 30,
	3, 30, 2, 3, 30, 31, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28,
	30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 2, 10, 4, 2,
	32, 40, 42, 42, 3, 2, 3, 4, 3, 2, 5, 6, 3, 2, 38, 40, 3, 2, 12, 14, 3,
	2, 15, 16, 3, 2, 17, 22, 3, 2, 28, 29, 2, 258, 2, 60, 3, 2, 2, 2, 4, 62,
	3, 2, 2, 2, 6, 64, 3, 2, 2, 2, 8, 66, 3, 2, 2, 2, 10, 70, 3, 2, 2, 2, 12,
	88, 3, 2, 2, 2, 14, 93, 3, 2, 2, 2, 16, 97, 3, 2, 2, 2, 18, 104, 3, 2,
	2, 2, 20, 109, 3, 2, 2, 2, 22, 132, 3, 2, 2, 2, 24, 144, 3, 2, 2, 2, 26,
	146, 3, 2, 2, 2, 28, 150, 3, 2, 2, 2, 30, 156, 3, 2, 2, 2, 32, 187, 3,
	2, 2, 2, 34, 189, 3, 2, 2, 2, 36, 196, 3, 2, 2, 2, 38, 198, 3, 2, 2, 2,
	40, 201, 3, 2, 2, 2, 42, 206, 3, 2, 2, 2, 44, 212, 3, 2, 2, 2, 46, 229,
	3, 2, 2, 2, 48, 231, 3, 2, 2, 2, 50, 241, 3, 2, 2, 2, 52, 243, 3, 2, 2,
	2, 54, 246, 3, 2, 2, 2, 56, 250, 3, 2, 2, 2, 58, 253, 3, 2, 2, 2, 60, 61,
	9, 2, 2, 2, 61, 3, 3, 2, 2, 2, 62, 63, 9, 3, 2, 2, 63, 5, 3, 2, 2, 2, 64,
	65, 9, 4, 2, 2, 65, 7, 3, 2, 2, 2, 66, 67, 5, 4, 3, 2, 67, 68, 7, 34, 2,
	2, 68, 69, 5, 6, 4, 2, 69, 9, 3, 2, 2, 2, 70, 71, 5, 4, 3, 2, 71, 72, 7,
	32, 2, 2, 72, 75, 5, 2, 2, 2, 73, 74, 7, 7, 2, 2, 74, 76, 5, 2, 2, 2, 75,
	73, 3, 2, 2, 2, 75, 76, 3, 2, 2, 2, 76, 81, 3, 2, 2, 2, 77, 78, 7, 7, 2,
	2, 78, 80, 9, 5, 2, 2, 79, 77, 3, 2, 2, 2, 80, 83, 3, 2, 2, 2, 81, 79,
	3, 2, 2, 2, 81, 82, 3, 2, 2, 2, 82, 84, 3, 2, 2, 2, 83, 81, 3, 2, 2, 2,
	84, 85, 7, 33, 2, 2, 85, 86, 5, 30, 16, 2, 86, 87, 5, 6, 4, 2, 87, 11,
	3, 2, 2, 2, 88, 89, 5, 4, 3, 2, 89, 90, 7, 35, 2, 2, 90, 91, 5, 30, 16,
	2, 91, 92, 5, 6, 4, 2, 92, 13, 3, 2, 2, 2, 93, 94, 5, 4, 3, 2, 94, 95,
	7, 37, 2, 2, 95, 96, 5, 6, 4, 2, 96, 15, 3, 2, 2, 2, 97, 98, 5, 4, 3, 2,
	98, 100, 7, 36, 2, 2, 99, 101, 5, 30, 16, 2, 100, 99, 3, 2, 2, 2, 100,
	101, 3, 2, 2, 2, 101, 102, 3, 2, 2, 2, 102, 103, 5, 6, 4, 2, 103, 17, 3,
	2, 2, 2, 104, 105, 7, 8, 2, 2, 105, 106, 5, 4, 3, 2, 106, 107, 5, 30, 16,
	2, 107, 108, 5, 6, 4, 2, 108, 19, 3, 2, 2, 2, 109, 110, 7, 9, 2, 2, 110,
	117, 5, 4, 3, 2, 111, 112, 7, 42, 2, 2, 112, 114, 7, 44, 2, 2, 113, 111,
	3, 2, 2, 2, 113, 114, 3, 2, 2, 2, 114, 115, 3, 2, 2, 2, 115, 116, 7, 42,
	2, 2, 116, 118, 7, 10, 2, 2, 117, 113, 3, 2, 2, 2, 117, 118, 3, 2, 2, 2,
	118, 119, 3, 2, 2, 2, 119, 128, 5, 30, 16, 2, 120, 125, 5, 22, 12, 2, 121,
	122, 7, 7, 2, 2, 122, 124, 5, 22, 12, 2, 123, 121, 3, 2, 2, 2, 124, 127,
	3, 2, 2, 2, 125, 123, 3, 2, 2, 2, 125, 126, 3, 2, 2, 2, 126, 129, 3, 2,
	2, 2, 127, 125, 3, 2, 2, 2, 128, 120, 3, 2, 2, 2, 128, 129, 3, 2, 2, 2,
	129, 130, 3, 2, 2, 2, 130, 131, 5, 6, 4, 2, 131, 21, 3, 2, 2, 2, 132, 133,
	7, 42, 2, 2, 133, 134, 7, 11, 2, 2, 134, 135, 5, 30, 16, 2, 135, 23, 3,
	2, 2, 2, 136, 141, 5, 26, 14, 2, 137, 138, 7, 7, 2, 2, 138, 140, 5, 26,
	14, 2, 139, 137, 3, 2, 2, 2, 140, 143, 3, 2, 2, 2, 141, 139, 3, 2, 2, 2,
	141, 142, 3, 2, 2, 2, 142, 145, 3, 2, 2, 2, 143, 141, 3, 2, 2, 2, 144,
	136, 3, 2, 2, 2, 144, 145, 3, 2, 2, 2, 145, 25, 3, 2, 2, 2, 146, 147, 7,
	42, 2, 2, 147, 148, 7, 11, 2, 2, 148, 149, 5, 36, 19, 2, 149, 27, 3, 2,
	2, 2, 150, 151, 5, 30, 16, 2, 151, 152, 7, 2, 2, 3, 152, 29, 3, 2, 2, 2,
	153, 154, 8, 16, 1, 2, 154, 157, 5, 32, 17, 2, 155, 157, 5, 34, 18, 2,
	156, 153, 3, 2, 2, 2, 156, 155, 3, 2, 2, 2, 157, 178, 3, 2, 2, 2, 158,
	159, 12, 8, 2, 2, 159, 160, 9, 6, 2, 2, 160, 177, 5, 30, 16, 9, 161, 162,
	12, 7, 2, 2, 162, 163, 9, 7, 2, 2, 163, 177, 5, 30, 16, 8, 164, 165, 12,
	6, 2, 2, 165, 166, 9, 8, 2, 2, 166, 177, 5, 30, 16, 7, 167, 168, 12, 5,
	2, 2, 168, 169, 7, 23, 2, 2, 169, 177, 5, 30, 16, 6, 170, 171, 12, 4, 2,
	2, 171, 172, 7, 24, 2, 2, 172, 177, 5, 30, 16, 5, 173, 174, 12, 3, 2, 2,
	174, 175, 7, 25, 2, 2, 175, 177, 7, 42, 2, 2, 176, 158, 3, 2, 2, 2, 176,
	161, 3, 2, 2, 2, 176, 164, 3, 2, 2, 2, 176, 167, 3, 2, 2, 2, 176, 170,
	3, 2, 2, 2, 176, 173, 3, 2, 2, 2, 177, 180, 3, 2, 2, 2, 178, 176, 3, 2,
	2, 2, 178, 179, 3, 2, 2, 2, 179, 31, 3, 2, 2, 2, 180, 178, 3, 2, 2, 2,
	181, 188, 5, 36, 19, 2, 182, 188, 5, 46, 24, 2, 183, 184, 7, 26, 2, 2,
	184, 185, 5, 30, 16, 2, 185, 186, 7, 27, 2, 2, 186, 188, 3, 2, 2, 2, 187,
	181, 3, 2, 2, 2, 187, 182, 3, 2, 2, 2, 187, 183, 3, 2, 2, 2, 188, 33, 3,
	2, 2, 2, 189, 190, 7, 45, 2, 2, 190, 191, 5, 30, 16, 2, 191, 35, 3, 2,
	2, 2, 192, 197, 5, 38, 20, 2, 193, 197, 5, 40, 21, 2, 194, 197, 5, 42,
	22, 2, 195, 197, 5, 44, 23, 2, 196, 192, 3, 2, 2, 2, 196, 193, 3, 2, 2,
	2, 196, 194, 3, 2, 2, 2, 196, 195, 3, 2, 2, 2, 197, 37, 3, 2, 2, 2, 198,
	199, 9, 9, 2, 2, 199, 39, 3, 2, 2, 2, 200, 202, 7, 16, 2, 2, 201, 200,
	3, 2, 2, 2, 201, 202, 3, 2, 2, 2, 202, 203, 3, 2, 2, 2, 203, 204, 7, 43,
	2, 2, 204, 41, 3, 2, 2, 2, 205, 207, 7, 16, 2, 2, 206, 205, 3, 2, 2, 2,
	206, 207, 3, 2, 2, 2, 207, 208, 3, 2, 2, 2, 208, 209, 7, 43, 2, 2, 209,
	210, 7, 44, 2, 2, 210, 211, 7, 43, 2, 2, 211, 43, 3, 2, 2, 2, 212, 213,
	7, 41, 2, 2, 213, 45, 3, 2, 2, 2, 214, 227, 5, 58, 30, 2, 215, 217, 5,
	58, 30, 2, 216, 218, 5, 50, 26, 2, 217, 216, 3, 2, 2, 2, 218, 219, 3, 2,
	2, 2, 219, 217, 3, 2, 2, 2, 219, 220, 3, 2, 2, 2, 220, 227, 3, 2, 2, 2,
	221, 223, 5, 50, 26, 2, 222, 221, 3, 2, 2, 2, 223, 224, 3, 2, 2, 2, 224,
	222, 3, 2, 2, 2, 224, 225, 3, 2, 2, 2, 225, 227, 3, 2, 2, 2, 226, 214,
	3, 2, 2, 2, 226, 215, 3, 2, 2, 2, 226, 222, 3, 2, 2, 2, 227, 230, 3, 2,
	2, 2, 228, 230, 5, 48, 25, 2, 229, 226, 3, 2, 2, 2, 229, 228, 3, 2, 2,
	2, 230, 47, 3, 2, 2, 2, 231, 236, 7, 44, 2, 2, 232, 234, 5, 54, 28, 2,
	233, 235, 5, 46, 24, 2, 234, 233, 3, 2, 2, 2, 234, 235, 3, 2, 2, 2, 235,
	237, 3, 2, 2, 2, 236, 232, 3, 2, 2, 2, 236, 237, 3, 2, 2, 2, 237, 49, 3,
	2, 2, 2, 238, 242, 5, 52, 27, 2, 239, 242, 5, 54, 28, 2, 240, 242, 5, 56,
	29, 2, 241, 238, 3, 2, 2, 2, 241, 239, 3, 2, 2, 2, 241, 240, 3, 2, 2, 2,
	242, 51, 3, 2, 2, 2, 243, 244, 7, 44, 2, 2, 244, 245, 5, 2, 2, 2, 245,
	53, 3, 2, 2, 2, 246, 247, 7, 30, 2, 2, 247, 248, 7, 43, 2, 2, 248, 249,
	7, 31, 2, 2, 249, 55, 3, 2, 2, 2, 250, 251, 7, 26, 2, 2, 251, 252, 7, 27,
	2, 2, 252, 57, 3, 2, 2, 2, 253, 254, 5, 2, 2, 2, 254, 59, 3, 2, 2, 2, 25,
	75, 81, 100, 113, 117, 125, 128, 141, 144, 156, 176, 178, 187, 196, 201,
	206, 219, 224, 226, 229, 234, 236, 241,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'{-'", "'{'", "'-}'", "'}'", "','", "'$'", "'@'", "':'", "'='", "'*'",
	"'/'", "'%'", "'+'", "'-'", "'=='", "'!='", "'<'", "'<='", "'>'", "'>='",
	"'&&'", "'||'", "'|'", "'('", "')'", "'true'", "'false'", "'['", "']'",
	"'for'", "'in'", "'end'", "'if'", "'indent'", "'else'", "'first'", "'last'",
	"'even'", "", "", "", "'.'", "'!'",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "", "", "", "", "", "", "", "FOR", "IN", "END", "IF",
	"INDENT", "ELSE", "FIRST", "LAST", "EVEN", "STRING", "IDENTIFIER", "INT",
	"DOT", "NOT", "WS",
}

var ruleNames = []string{
	"identifier", "opn", "cls", "end", "forBlock", "ifBlock", "elseBlock",
	"indentBlock", "echo", "view", "viewParam", "visitDefaultParameters", "defaultParam",
	"rootExpression", "expression", "operand", "unaryExpression", "literal",
	"boolLiteral", "intLiteral", "floatLiteral", "stringLiteral", "selectorChain",
	"rootSelector", "selector", "fieldSelector", "index", "call", "variable",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type GoGenTemplateParser struct {
	*antlr.BaseParser
}

func NewGoGenTemplateParser(input antlr.TokenStream) *GoGenTemplateParser {
	this := new(GoGenTemplateParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "GoGenTemplate.g4"

	return this
}

// GoGenTemplateParser tokens.
const (
	GoGenTemplateParserEOF        = antlr.TokenEOF
	GoGenTemplateParserT__0       = 1
	GoGenTemplateParserT__1       = 2
	GoGenTemplateParserT__2       = 3
	GoGenTemplateParserT__3       = 4
	GoGenTemplateParserT__4       = 5
	GoGenTemplateParserT__5       = 6
	GoGenTemplateParserT__6       = 7
	GoGenTemplateParserT__7       = 8
	GoGenTemplateParserT__8       = 9
	GoGenTemplateParserT__9       = 10
	GoGenTemplateParserT__10      = 11
	GoGenTemplateParserT__11      = 12
	GoGenTemplateParserT__12      = 13
	GoGenTemplateParserT__13      = 14
	GoGenTemplateParserT__14      = 15
	GoGenTemplateParserT__15      = 16
	GoGenTemplateParserT__16      = 17
	GoGenTemplateParserT__17      = 18
	GoGenTemplateParserT__18      = 19
	GoGenTemplateParserT__19      = 20
	GoGenTemplateParserT__20      = 21
	GoGenTemplateParserT__21      = 22
	GoGenTemplateParserT__22      = 23
	GoGenTemplateParserT__23      = 24
	GoGenTemplateParserT__24      = 25
	GoGenTemplateParserT__25      = 26
	GoGenTemplateParserT__26      = 27
	GoGenTemplateParserT__27      = 28
	GoGenTemplateParserT__28      = 29
	GoGenTemplateParserFOR        = 30
	GoGenTemplateParserIN         = 31
	GoGenTemplateParserEND        = 32
	GoGenTemplateParserIF         = 33
	GoGenTemplateParserINDENT     = 34
	GoGenTemplateParserELSE       = 35
	GoGenTemplateParserFIRST      = 36
	GoGenTemplateParserLAST       = 37
	GoGenTemplateParserEVEN       = 38
	GoGenTemplateParserSTRING     = 39
	GoGenTemplateParserIDENTIFIER = 40
	GoGenTemplateParserINT        = 41
	GoGenTemplateParserDOT        = 42
	GoGenTemplateParserNOT        = 43
	GoGenTemplateParserWS         = 44
)

// GoGenTemplateParser rules.
const (
	GoGenTemplateParserRULE_identifier             = 0
	GoGenTemplateParserRULE_opn                    = 1
	GoGenTemplateParserRULE_cls                    = 2
	GoGenTemplateParserRULE_end                    = 3
	GoGenTemplateParserRULE_forBlock               = 4
	GoGenTemplateParserRULE_ifBlock                = 5
	GoGenTemplateParserRULE_elseBlock              = 6
	GoGenTemplateParserRULE_indentBlock            = 7
	GoGenTemplateParserRULE_echo                   = 8
	GoGenTemplateParserRULE_view                   = 9
	GoGenTemplateParserRULE_viewParam              = 10
	GoGenTemplateParserRULE_visitDefaultParameters = 11
	GoGenTemplateParserRULE_defaultParam           = 12
	GoGenTemplateParserRULE_rootExpression         = 13
	GoGenTemplateParserRULE_expression             = 14
	GoGenTemplateParserRULE_operand                = 15
	GoGenTemplateParserRULE_unaryExpression        = 16
	GoGenTemplateParserRULE_literal                = 17
	GoGenTemplateParserRULE_boolLiteral            = 18
	GoGenTemplateParserRULE_intLiteral             = 19
	GoGenTemplateParserRULE_floatLiteral           = 20
	GoGenTemplateParserRULE_stringLiteral          = 21
	GoGenTemplateParserRULE_selectorChain          = 22
	GoGenTemplateParserRULE_rootSelector           = 23
	GoGenTemplateParserRULE_selector               = 24
	GoGenTemplateParserRULE_fieldSelector          = 25
	GoGenTemplateParserRULE_index                  = 26
	GoGenTemplateParserRULE_call                   = 27
	GoGenTemplateParserRULE_variable               = 28
)

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_identifier
	return p
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *IdentifierContext) FOR() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFOR, 0)
}

func (s *IdentifierContext) IN() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIN, 0)
}

func (s *IdentifierContext) END() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEND, 0)
}

func (s *IdentifierContext) IF() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIF, 0)
}

func (s *IdentifierContext) ELSE() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserELSE, 0)
}

func (s *IdentifierContext) FIRST() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFIRST, 0)
}

func (s *IdentifierContext) LAST() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserLAST, 0)
}

func (s *IdentifierContext) EVEN() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEVEN, 0)
}

func (s *IdentifierContext) INDENT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINDENT, 0)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Identifier() (localctx IIdentifierContext) {
	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, GoGenTemplateParserRULE_identifier)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(58)
		_la = p.GetTokenStream().LA(1)

		if !(((_la-30)&-(0x1f+1)) == 0 && ((1<<uint((_la-30)))&((1<<(GoGenTemplateParserFOR-30))|(1<<(GoGenTemplateParserIN-30))|(1<<(GoGenTemplateParserEND-30))|(1<<(GoGenTemplateParserIF-30))|(1<<(GoGenTemplateParserINDENT-30))|(1<<(GoGenTemplateParserELSE-30))|(1<<(GoGenTemplateParserFIRST-30))|(1<<(GoGenTemplateParserLAST-30))|(1<<(GoGenTemplateParserEVEN-30))|(1<<(GoGenTemplateParserIDENTIFIER-30)))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IOpnContext is an interface to support dynamic dispatch.
type IOpnContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOpnContext differentiates from other interfaces.
	IsOpnContext()
}

type OpnContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOpnContext() *OpnContext {
	var p = new(OpnContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_opn
	return p
}

func (*OpnContext) IsOpnContext() {}

func NewOpnContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OpnContext {
	var p = new(OpnContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_opn

	return p
}

func (s *OpnContext) GetParser() antlr.Parser { return s.parser }
func (s *OpnContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OpnContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OpnContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitOpn(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Opn() (localctx IOpnContext) {
	localctx = NewOpnContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, GoGenTemplateParserRULE_opn)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(60)
		_la = p.GetTokenStream().LA(1)

		if !(_la == GoGenTemplateParserT__0 || _la == GoGenTemplateParserT__1) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IClsContext is an interface to support dynamic dispatch.
type IClsContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsClsContext differentiates from other interfaces.
	IsClsContext()
}

type ClsContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyClsContext() *ClsContext {
	var p = new(ClsContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_cls
	return p
}

func (*ClsContext) IsClsContext() {}

func NewClsContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ClsContext {
	var p = new(ClsContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_cls

	return p
}

func (s *ClsContext) GetParser() antlr.Parser { return s.parser }
func (s *ClsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ClsContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ClsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitCls(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Cls() (localctx IClsContext) {
	localctx = NewClsContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, GoGenTemplateParserRULE_cls)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(62)
		_la = p.GetTokenStream().LA(1)

		if !(_la == GoGenTemplateParserT__2 || _la == GoGenTemplateParserT__3) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IEndContext is an interface to support dynamic dispatch.
type IEndContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsEndContext differentiates from other interfaces.
	IsEndContext()
}

type EndContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEndContext() *EndContext {
	var p = new(EndContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_end
	return p
}

func (*EndContext) IsEndContext() {}

func NewEndContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EndContext {
	var p = new(EndContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_end

	return p
}

func (s *EndContext) GetParser() antlr.Parser { return s.parser }

func (s *EndContext) Opn() IOpnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *EndContext) END() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEND, 0)
}

func (s *EndContext) Cls() IClsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IClsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *EndContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EndContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EndContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitEnd(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) End() (localctx IEndContext) {
	localctx = NewEndContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, GoGenTemplateParserRULE_end)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(64)
		p.Opn()
	}
	{
		p.SetState(65)
		p.Match(GoGenTemplateParserEND)
	}
	{
		p.SetState(66)
		p.Cls()
	}

	return localctx
}

// IForBlockContext is an interface to support dynamic dispatch.
type IForBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetKey returns the key rule contexts.
	GetKey() IIdentifierContext

	// GetValue returns the value rule contexts.
	GetValue() IIdentifierContext

	// SetKey sets the key rule contexts.
	SetKey(IIdentifierContext)

	// SetValue sets the value rule contexts.
	SetValue(IIdentifierContext)

	// IsForBlockContext differentiates from other interfaces.
	IsForBlockContext()
}

type ForBlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	key    IIdentifierContext
	value  IIdentifierContext
}

func NewEmptyForBlockContext() *ForBlockContext {
	var p = new(ForBlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_forBlock
	return p
}

func (*ForBlockContext) IsForBlockContext() {}

func NewForBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ForBlockContext {
	var p = new(ForBlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_forBlock

	return p
}

func (s *ForBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *ForBlockContext) GetKey() IIdentifierContext { return s.key }

func (s *ForBlockContext) GetValue() IIdentifierContext { return s.value }

func (s *ForBlockContext) SetKey(v IIdentifierContext) { s.key = v }

func (s *ForBlockContext) SetValue(v IIdentifierContext) { s.value = v }

func (s *ForBlockContext) Opn() IOpnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *ForBlockContext) FOR() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFOR, 0)
}

func (s *ForBlockContext) IN() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIN, 0)
}

func (s *ForBlockContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ForBlockContext) Cls() IClsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IClsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *ForBlockContext) AllIdentifier() []IIdentifierContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IIdentifierContext)(nil)).Elem())
	var tst = make([]IIdentifierContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IIdentifierContext)
		}
	}

	return tst
}

func (s *ForBlockContext) Identifier(i int) IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ForBlockContext) AllFIRST() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserFIRST)
}

func (s *ForBlockContext) FIRST(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFIRST, i)
}

func (s *ForBlockContext) AllLAST() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserLAST)
}

func (s *ForBlockContext) LAST(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserLAST, i)
}

func (s *ForBlockContext) AllEVEN() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserEVEN)
}

func (s *ForBlockContext) EVEN(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEVEN, i)
}

func (s *ForBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ForBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ForBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitForBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ForBlock() (localctx IForBlockContext) {
	localctx = NewForBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, GoGenTemplateParserRULE_forBlock)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(68)
		p.Opn()
	}
	{
		p.SetState(69)
		p.Match(GoGenTemplateParserFOR)
	}
	{
		p.SetState(70)

		var _x = p.Identifier()

		localctx.(*ForBlockContext).key = _x
	}
	p.SetState(73)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 0, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(71)
			p.Match(GoGenTemplateParserT__4)
		}
		{
			p.SetState(72)

			var _x = p.Identifier()

			localctx.(*ForBlockContext).value = _x
		}

	}
	p.SetState(79)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == GoGenTemplateParserT__4 {
		{
			p.SetState(75)
			p.Match(GoGenTemplateParserT__4)
		}
		{
			p.SetState(76)
			_la = p.GetTokenStream().LA(1)

			if !(((_la-36)&-(0x1f+1)) == 0 && ((1<<uint((_la-36)))&((1<<(GoGenTemplateParserFIRST-36))|(1<<(GoGenTemplateParserLAST-36))|(1<<(GoGenTemplateParserEVEN-36)))) != 0) {
				p.GetErrorHandler().RecoverInline(p)
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}

		p.SetState(81)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(82)
		p.Match(GoGenTemplateParserIN)
	}
	{
		p.SetState(83)
		p.expression(0)
	}
	{
		p.SetState(84)
		p.Cls()
	}

	return localctx
}

// IIfBlockContext is an interface to support dynamic dispatch.
type IIfBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIfBlockContext differentiates from other interfaces.
	IsIfBlockContext()
}

type IfBlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIfBlockContext() *IfBlockContext {
	var p = new(IfBlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_ifBlock
	return p
}

func (*IfBlockContext) IsIfBlockContext() {}

func NewIfBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IfBlockContext {
	var p = new(IfBlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_ifBlock

	return p
}

func (s *IfBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *IfBlockContext) Opn() IOpnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *IfBlockContext) IF() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIF, 0)
}

func (s *IfBlockContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IfBlockContext) Cls() IClsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IClsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *IfBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IfBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIfBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) IfBlock() (localctx IIfBlockContext) {
	localctx = NewIfBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, GoGenTemplateParserRULE_ifBlock)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(86)
		p.Opn()
	}
	{
		p.SetState(87)
		p.Match(GoGenTemplateParserIF)
	}
	{
		p.SetState(88)
		p.expression(0)
	}
	{
		p.SetState(89)
		p.Cls()
	}

	return localctx
}

// IElseBlockContext is an interface to support dynamic dispatch.
type IElseBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsElseBlockContext differentiates from other interfaces.
	IsElseBlockContext()
}

type ElseBlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyElseBlockContext() *ElseBlockContext {
	var p = new(ElseBlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_elseBlock
	return p
}

func (*ElseBlockContext) IsElseBlockContext() {}

func NewElseBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ElseBlockContext {
	var p = new(ElseBlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_elseBlock

	return p
}

func (s *ElseBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *ElseBlockContext) Opn() IOpnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *ElseBlockContext) ELSE() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserELSE, 0)
}

func (s *ElseBlockContext) Cls() IClsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IClsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *ElseBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ElseBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ElseBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitElseBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ElseBlock() (localctx IElseBlockContext) {
	localctx = NewElseBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, GoGenTemplateParserRULE_elseBlock)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(91)
		p.Opn()
	}
	{
		p.SetState(92)
		p.Match(GoGenTemplateParserELSE)
	}
	{
		p.SetState(93)
		p.Cls()
	}

	return localctx
}

// IIndentBlockContext is an interface to support dynamic dispatch.
type IIndentBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIndentBlockContext differentiates from other interfaces.
	IsIndentBlockContext()
}

type IndentBlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIndentBlockContext() *IndentBlockContext {
	var p = new(IndentBlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_indentBlock
	return p
}

func (*IndentBlockContext) IsIndentBlockContext() {}

func NewIndentBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IndentBlockContext {
	var p = new(IndentBlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_indentBlock

	return p
}

func (s *IndentBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *IndentBlockContext) Opn() IOpnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *IndentBlockContext) INDENT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINDENT, 0)
}

func (s *IndentBlockContext) Cls() IClsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IClsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *IndentBlockContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IndentBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IndentBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IndentBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIndentBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) IndentBlock() (localctx IIndentBlockContext) {
	localctx = NewIndentBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, GoGenTemplateParserRULE_indentBlock)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(95)
		p.Opn()
	}
	{
		p.SetState(96)
		p.Match(GoGenTemplateParserINDENT)
	}
	p.SetState(98)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if ((_la-14)&-(0x1f+1)) == 0 && ((1<<uint((_la-14)))&((1<<(GoGenTemplateParserT__13-14))|(1<<(GoGenTemplateParserT__23-14))|(1<<(GoGenTemplateParserT__25-14))|(1<<(GoGenTemplateParserT__26-14))|(1<<(GoGenTemplateParserT__27-14))|(1<<(GoGenTemplateParserFOR-14))|(1<<(GoGenTemplateParserIN-14))|(1<<(GoGenTemplateParserEND-14))|(1<<(GoGenTemplateParserIF-14))|(1<<(GoGenTemplateParserINDENT-14))|(1<<(GoGenTemplateParserELSE-14))|(1<<(GoGenTemplateParserFIRST-14))|(1<<(GoGenTemplateParserLAST-14))|(1<<(GoGenTemplateParserEVEN-14))|(1<<(GoGenTemplateParserSTRING-14))|(1<<(GoGenTemplateParserIDENTIFIER-14))|(1<<(GoGenTemplateParserINT-14))|(1<<(GoGenTemplateParserDOT-14))|(1<<(GoGenTemplateParserNOT-14)))) != 0 {
		{
			p.SetState(97)
			p.expression(0)
		}

	}
	{
		p.SetState(100)
		p.Cls()
	}

	return localctx
}

// IEchoContext is an interface to support dynamic dispatch.
type IEchoContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsEchoContext differentiates from other interfaces.
	IsEchoContext()
}

type EchoContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEchoContext() *EchoContext {
	var p = new(EchoContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_echo
	return p
}

func (*EchoContext) IsEchoContext() {}

func NewEchoContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EchoContext {
	var p = new(EchoContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_echo

	return p
}

func (s *EchoContext) GetParser() antlr.Parser { return s.parser }

func (s *EchoContext) Opn() IOpnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *EchoContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *EchoContext) Cls() IClsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IClsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *EchoContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EchoContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EchoContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitEcho(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Echo() (localctx IEchoContext) {
	localctx = NewEchoContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, GoGenTemplateParserRULE_echo)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(102)
		p.Match(GoGenTemplateParserT__5)
	}
	{
		p.SetState(103)
		p.Opn()
	}
	{
		p.SetState(104)
		p.expression(0)
	}
	{
		p.SetState(105)
		p.Cls()
	}

	return localctx
}

// IViewContext is an interface to support dynamic dispatch.
type IViewContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetPkg returns the pkg token.
	GetPkg() antlr.Token

	// GetName returns the name token.
	GetName() antlr.Token

	// SetPkg sets the pkg token.
	SetPkg(antlr.Token)

	// SetName sets the name token.
	SetName(antlr.Token)

	// IsViewContext differentiates from other interfaces.
	IsViewContext()
}

type ViewContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	pkg    antlr.Token
	name   antlr.Token
}

func NewEmptyViewContext() *ViewContext {
	var p = new(ViewContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_view
	return p
}

func (*ViewContext) IsViewContext() {}

func NewViewContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ViewContext {
	var p = new(ViewContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_view

	return p
}

func (s *ViewContext) GetParser() antlr.Parser { return s.parser }

func (s *ViewContext) GetPkg() antlr.Token { return s.pkg }

func (s *ViewContext) GetName() antlr.Token { return s.name }

func (s *ViewContext) SetPkg(v antlr.Token) { s.pkg = v }

func (s *ViewContext) SetName(v antlr.Token) { s.name = v }

func (s *ViewContext) Opn() IOpnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *ViewContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ViewContext) Cls() IClsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IClsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *ViewContext) AllViewParam() []IViewParamContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IViewParamContext)(nil)).Elem())
	var tst = make([]IViewParamContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IViewParamContext)
		}
	}

	return tst
}

func (s *ViewContext) ViewParam(i int) IViewParamContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IViewParamContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IViewParamContext)
}

func (s *ViewContext) AllIDENTIFIER() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserIDENTIFIER)
}

func (s *ViewContext) IDENTIFIER(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, i)
}

func (s *ViewContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *ViewContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ViewContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ViewContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitView(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) View() (localctx IViewContext) {
	localctx = NewViewContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, GoGenTemplateParserRULE_view)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(107)
		p.Match(GoGenTemplateParserT__6)
	}
	{
		p.SetState(108)
		p.Opn()
	}
	p.SetState(115)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 4, p.GetParserRuleContext()) == 1 {
		p.SetState(111)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 3, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(109)

				var _m = p.Match(GoGenTemplateParserIDENTIFIER)

				localctx.(*ViewContext).pkg = _m
			}
			{
				p.SetState(110)
				p.Match(GoGenTemplateParserDOT)
			}

		}
		{
			p.SetState(113)

			var _m = p.Match(GoGenTemplateParserIDENTIFIER)

			localctx.(*ViewContext).name = _m
		}
		{
			p.SetState(114)
			p.Match(GoGenTemplateParserT__7)
		}

	}
	{
		p.SetState(117)
		p.expression(0)
	}
	p.SetState(126)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserIDENTIFIER {
		{
			p.SetState(118)
			p.ViewParam()
		}
		p.SetState(123)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == GoGenTemplateParserT__4 {
			{
				p.SetState(119)
				p.Match(GoGenTemplateParserT__4)
			}
			{
				p.SetState(120)
				p.ViewParam()
			}

			p.SetState(125)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}

	}
	{
		p.SetState(128)
		p.Cls()
	}

	return localctx
}

// IViewParamContext is an interface to support dynamic dispatch.
type IViewParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsViewParamContext differentiates from other interfaces.
	IsViewParamContext()
}

type ViewParamContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyViewParamContext() *ViewParamContext {
	var p = new(ViewParamContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_viewParam
	return p
}

func (*ViewParamContext) IsViewParamContext() {}

func NewViewParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ViewParamContext {
	var p = new(ViewParamContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_viewParam

	return p
}

func (s *ViewParamContext) GetParser() antlr.Parser { return s.parser }

func (s *ViewParamContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *ViewParamContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ViewParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ViewParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ViewParamContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitViewParam(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ViewParam() (localctx IViewParamContext) {
	localctx = NewViewParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, GoGenTemplateParserRULE_viewParam)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(130)
		p.Match(GoGenTemplateParserIDENTIFIER)
	}
	{
		p.SetState(131)
		p.Match(GoGenTemplateParserT__8)
	}
	{
		p.SetState(132)
		p.expression(0)
	}

	return localctx
}

// IVisitDefaultParametersContext is an interface to support dynamic dispatch.
type IVisitDefaultParametersContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsVisitDefaultParametersContext differentiates from other interfaces.
	IsVisitDefaultParametersContext()
}

type VisitDefaultParametersContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyVisitDefaultParametersContext() *VisitDefaultParametersContext {
	var p = new(VisitDefaultParametersContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_visitDefaultParameters
	return p
}

func (*VisitDefaultParametersContext) IsVisitDefaultParametersContext() {}

func NewVisitDefaultParametersContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *VisitDefaultParametersContext {
	var p = new(VisitDefaultParametersContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_visitDefaultParameters

	return p
}

func (s *VisitDefaultParametersContext) GetParser() antlr.Parser { return s.parser }

func (s *VisitDefaultParametersContext) AllDefaultParam() []IDefaultParamContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IDefaultParamContext)(nil)).Elem())
	var tst = make([]IDefaultParamContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IDefaultParamContext)
		}
	}

	return tst
}

func (s *VisitDefaultParametersContext) DefaultParam(i int) IDefaultParamContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDefaultParamContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IDefaultParamContext)
}

func (s *VisitDefaultParametersContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *VisitDefaultParametersContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *VisitDefaultParametersContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitVisitDefaultParameters(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) VisitDefaultParameters() (localctx IVisitDefaultParametersContext) {
	localctx = NewVisitDefaultParametersContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, GoGenTemplateParserRULE_visitDefaultParameters)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(142)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserIDENTIFIER {
		{
			p.SetState(134)
			p.DefaultParam()
		}
		p.SetState(139)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == GoGenTemplateParserT__4 {
			{
				p.SetState(135)
				p.Match(GoGenTemplateParserT__4)
			}
			{
				p.SetState(136)
				p.DefaultParam()
			}

			p.SetState(141)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}

	}

	return localctx
}

// IDefaultParamContext is an interface to support dynamic dispatch.
type IDefaultParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDefaultParamContext differentiates from other interfaces.
	IsDefaultParamContext()
}

type DefaultParamContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDefaultParamContext() *DefaultParamContext {
	var p = new(DefaultParamContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_defaultParam
	return p
}

func (*DefaultParamContext) IsDefaultParamContext() {}

func NewDefaultParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DefaultParamContext {
	var p = new(DefaultParamContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_defaultParam

	return p
}

func (s *DefaultParamContext) GetParser() antlr.Parser { return s.parser }

func (s *DefaultParamContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *DefaultParamContext) Literal() ILiteralContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ILiteralContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ILiteralContext)
}

func (s *DefaultParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DefaultParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DefaultParamContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitDefaultParam(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) DefaultParam() (localctx IDefaultParamContext) {
	localctx = NewDefaultParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, GoGenTemplateParserRULE_defaultParam)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(144)
		p.Match(GoGenTemplateParserIDENTIFIER)
	}
	{
		p.SetState(145)
		p.Match(GoGenTemplateParserT__8)
	}
	{
		p.SetState(146)
		p.Literal()
	}

	return localctx
}

// IRootExpressionContext is an interface to support dynamic dispatch.
type IRootExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsRootExpressionContext differentiates from other interfaces.
	IsRootExpressionContext()
}

type RootExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootExpressionContext() *RootExpressionContext {
	var p = new(RootExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_rootExpression
	return p
}

func (*RootExpressionContext) IsRootExpressionContext() {}

func NewRootExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootExpressionContext {
	var p = new(RootExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_rootExpression

	return p
}

func (s *RootExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *RootExpressionContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *RootExpressionContext) EOF() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEOF, 0)
}

func (s *RootExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitRootExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) RootExpression() (localctx IRootExpressionContext) {
	localctx = NewRootExpressionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, GoGenTemplateParserRULE_rootExpression)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(148)
		p.expression(0)
	}
	{
		p.SetState(149)
		p.Match(GoGenTemplateParserEOF)
	}

	return localctx
}

// IExpressionContext is an interface to support dynamic dispatch.
type IExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetOperator returns the operator token.
	GetOperator() antlr.Token

	// GetFilter returns the filter token.
	GetFilter() antlr.Token

	// SetOperator sets the operator token.
	SetOperator(antlr.Token)

	// SetFilter sets the filter token.
	SetFilter(antlr.Token)

	// IsExpressionContext differentiates from other interfaces.
	IsExpressionContext()
}

type ExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser   antlr.Parser
	operator antlr.Token
	filter   antlr.Token
}

func NewEmptyExpressionContext() *ExpressionContext {
	var p = new(ExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_expression
	return p
}

func (*ExpressionContext) IsExpressionContext() {}

func NewExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExpressionContext {
	var p = new(ExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_expression

	return p
}

func (s *ExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *ExpressionContext) GetOperator() antlr.Token { return s.operator }

func (s *ExpressionContext) GetFilter() antlr.Token { return s.filter }

func (s *ExpressionContext) SetOperator(v antlr.Token) { s.operator = v }

func (s *ExpressionContext) SetFilter(v antlr.Token) { s.filter = v }

func (s *ExpressionContext) Operand() IOperandContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOperandContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOperandContext)
}

func (s *ExpressionContext) UnaryExpression() IUnaryExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUnaryExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IUnaryExpressionContext)
}

func (s *ExpressionContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *ExpressionContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ExpressionContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *ExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Expression() (localctx IExpressionContext) {
	return p.expression(0)
}

func (p *GoGenTemplateParser) expression(_p int) (localctx IExpressionContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()
	_parentState := p.GetState()
	localctx = NewExpressionContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExpressionContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 28
	p.EnterRecursionRule(localctx, 28, GoGenTemplateParserRULE_expression, _p)
	var _la int

	defer func() {
		p.UnrollRecursionContexts(_parentctx)
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(154)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case GoGenTemplateParserT__13, GoGenTemplateParserT__23, GoGenTemplateParserT__25, GoGenTemplateParserT__26, GoGenTemplateParserT__27, GoGenTemplateParserFOR, GoGenTemplateParserIN, GoGenTemplateParserEND, GoGenTemplateParserIF, GoGenTemplateParserINDENT, GoGenTemplateParserELSE, GoGenTemplateParserFIRST, GoGenTemplateParserLAST, GoGenTemplateParserEVEN, GoGenTemplateParserSTRING, GoGenTemplateParserIDENTIFIER, GoGenTemplateParserINT, GoGenTemplateParserDOT:
		{
			p.SetState(152)
			p.Operand()
		}

	case GoGenTemplateParserNOT:
		{
			p.SetState(153)
			p.UnaryExpression()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(176)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(174)
			p.GetErrorHandler().Sync(p)
			switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 10, p.GetParserRuleContext()) {
			case 1:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(156)

				if !(p.Precpred(p.GetParserRuleContext(), 6)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 6)", ""))
				}
				{
					p.SetState(157)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*ExpressionContext).operator = _lt

					_la = p.GetTokenStream().LA(1)

					if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<GoGenTemplateParserT__9)|(1<<GoGenTemplateParserT__10)|(1<<GoGenTemplateParserT__11))) != 0) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*ExpressionContext).operator = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				{
					p.SetState(158)
					p.expression(7)
				}

			case 2:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(159)

				if !(p.Precpred(p.GetParserRuleContext(), 5)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 5)", ""))
				}
				{
					p.SetState(160)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*ExpressionContext).operator = _lt

					_la = p.GetTokenStream().LA(1)

					if !(_la == GoGenTemplateParserT__12 || _la == GoGenTemplateParserT__13) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*ExpressionContext).operator = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				{
					p.SetState(161)
					p.expression(6)
				}

			case 3:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(162)

				if !(p.Precpred(p.GetParserRuleContext(), 4)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 4)", ""))
				}
				{
					p.SetState(163)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*ExpressionContext).operator = _lt

					_la = p.GetTokenStream().LA(1)

					if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<GoGenTemplateParserT__14)|(1<<GoGenTemplateParserT__15)|(1<<GoGenTemplateParserT__16)|(1<<GoGenTemplateParserT__17)|(1<<GoGenTemplateParserT__18)|(1<<GoGenTemplateParserT__19))) != 0) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*ExpressionContext).operator = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				{
					p.SetState(164)
					p.expression(5)
				}

			case 4:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(165)

				if !(p.Precpred(p.GetParserRuleContext(), 3)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 3)", ""))
				}
				{
					p.SetState(166)

					var _m = p.Match(GoGenTemplateParserT__20)

					localctx.(*ExpressionContext).operator = _m
				}
				{
					p.SetState(167)
					p.expression(4)
				}

			case 5:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(168)

				if !(p.Precpred(p.GetParserRuleContext(), 2)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 2)", ""))
				}
				{
					p.SetState(169)

					var _m = p.Match(GoGenTemplateParserT__21)

					localctx.(*ExpressionContext).operator = _m
				}
				{
					p.SetState(170)
					p.expression(3)
				}

			case 6:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(171)

				if !(p.Precpred(p.GetParserRuleContext(), 1)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 1)", ""))
				}
				{
					p.SetState(172)
					p.Match(GoGenTemplateParserT__22)
				}
				{
					p.SetState(173)

					var _m = p.Match(GoGenTemplateParserIDENTIFIER)

					localctx.(*ExpressionContext).filter = _m
				}

			}

		}
		p.SetState(178)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext())
	}

	return localctx
}

// IOperandContext is an interface to support dynamic dispatch.
type IOperandContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOperandContext differentiates from other interfaces.
	IsOperandContext()
}

type OperandContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOperandContext() *OperandContext {
	var p = new(OperandContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_operand
	return p
}

func (*OperandContext) IsOperandContext() {}

func NewOperandContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OperandContext {
	var p = new(OperandContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_operand

	return p
}

func (s *OperandContext) GetParser() antlr.Parser { return s.parser }

func (s *OperandContext) Literal() ILiteralContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ILiteralContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ILiteralContext)
}

func (s *OperandContext) SelectorChain() ISelectorChainContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISelectorChainContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISelectorChainContext)
}

func (s *OperandContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *OperandContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OperandContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OperandContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitOperand(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Operand() (localctx IOperandContext) {
	localctx = NewOperandContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, GoGenTemplateParserRULE_operand)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(185)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 12, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(179)
			p.Literal()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(180)
			p.SelectorChain()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(181)
			p.Match(GoGenTemplateParserT__23)
		}
		{
			p.SetState(182)
			p.expression(0)
		}
		{
			p.SetState(183)
			p.Match(GoGenTemplateParserT__24)
		}

	}

	return localctx
}

// IUnaryExpressionContext is an interface to support dynamic dispatch.
type IUnaryExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsUnaryExpressionContext differentiates from other interfaces.
	IsUnaryExpressionContext()
}

type UnaryExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUnaryExpressionContext() *UnaryExpressionContext {
	var p = new(UnaryExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_unaryExpression
	return p
}

func (*UnaryExpressionContext) IsUnaryExpressionContext() {}

func NewUnaryExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *UnaryExpressionContext {
	var p = new(UnaryExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_unaryExpression

	return p
}

func (s *UnaryExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *UnaryExpressionContext) NOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserNOT, 0)
}

func (s *UnaryExpressionContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *UnaryExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UnaryExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *UnaryExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitUnaryExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) UnaryExpression() (localctx IUnaryExpressionContext) {
	localctx = NewUnaryExpressionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, GoGenTemplateParserRULE_unaryExpression)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(187)
		p.Match(GoGenTemplateParserNOT)
	}
	{
		p.SetState(188)
		p.expression(0)
	}

	return localctx
}

// ILiteralContext is an interface to support dynamic dispatch.
type ILiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsLiteralContext differentiates from other interfaces.
	IsLiteralContext()
}

type LiteralContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLiteralContext() *LiteralContext {
	var p = new(LiteralContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_literal
	return p
}

func (*LiteralContext) IsLiteralContext() {}

func NewLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LiteralContext {
	var p = new(LiteralContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_literal

	return p
}

func (s *LiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *LiteralContext) BoolLiteral() IBoolLiteralContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBoolLiteralContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBoolLiteralContext)
}

func (s *LiteralContext) IntLiteral() IIntLiteralContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIntLiteralContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIntLiteralContext)
}

func (s *LiteralContext) FloatLiteral() IFloatLiteralContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFloatLiteralContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFloatLiteralContext)
}

func (s *LiteralContext) StringLiteral() IStringLiteralContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStringLiteralContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IStringLiteralContext)
}

func (s *LiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Literal() (localctx ILiteralContext) {
	localctx = NewLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, GoGenTemplateParserRULE_literal)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(194)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 13, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(190)
			p.BoolLiteral()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(191)
			p.IntLiteral()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(192)
			p.FloatLiteral()
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(193)
			p.StringLiteral()
		}

	}

	return localctx
}

// IBoolLiteralContext is an interface to support dynamic dispatch.
type IBoolLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBoolLiteralContext differentiates from other interfaces.
	IsBoolLiteralContext()
}

type BoolLiteralContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBoolLiteralContext() *BoolLiteralContext {
	var p = new(BoolLiteralContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_boolLiteral
	return p
}

func (*BoolLiteralContext) IsBoolLiteralContext() {}

func NewBoolLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BoolLiteralContext {
	var p = new(BoolLiteralContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_boolLiteral

	return p
}

func (s *BoolLiteralContext) GetParser() antlr.Parser { return s.parser }
func (s *BoolLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BoolLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitBoolLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) BoolLiteral() (localctx IBoolLiteralContext) {
	localctx = NewBoolLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, GoGenTemplateParserRULE_boolLiteral)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(196)
		_la = p.GetTokenStream().LA(1)

		if !(_la == GoGenTemplateParserT__25 || _la == GoGenTemplateParserT__26) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IIntLiteralContext is an interface to support dynamic dispatch.
type IIntLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIntLiteralContext differentiates from other interfaces.
	IsIntLiteralContext()
}

type IntLiteralContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIntLiteralContext() *IntLiteralContext {
	var p = new(IntLiteralContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_intLiteral
	return p
}

func (*IntLiteralContext) IsIntLiteralContext() {}

func NewIntLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IntLiteralContext {
	var p = new(IntLiteralContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_intLiteral

	return p
}

func (s *IntLiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *IntLiteralContext) INT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINT, 0)
}

func (s *IntLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IntLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IntLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIntLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) IntLiteral() (localctx IIntLiteralContext) {
	localctx = NewIntLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, GoGenTemplateParserRULE_intLiteral)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(199)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserT__13 {
		{
			p.SetState(198)
			p.Match(GoGenTemplateParserT__13)
		}

	}
	{
		p.SetState(201)
		p.Match(GoGenTemplateParserINT)
	}

	return localctx
}

// IFloatLiteralContext is an interface to support dynamic dispatch.
type IFloatLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFloatLiteralContext differentiates from other interfaces.
	IsFloatLiteralContext()
}

type FloatLiteralContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFloatLiteralContext() *FloatLiteralContext {
	var p = new(FloatLiteralContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_floatLiteral
	return p
}

func (*FloatLiteralContext) IsFloatLiteralContext() {}

func NewFloatLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FloatLiteralContext {
	var p = new(FloatLiteralContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_floatLiteral

	return p
}

func (s *FloatLiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *FloatLiteralContext) AllINT() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserINT)
}

func (s *FloatLiteralContext) INT(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINT, i)
}

func (s *FloatLiteralContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *FloatLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FloatLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FloatLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitFloatLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) FloatLiteral() (localctx IFloatLiteralContext) {
	localctx = NewFloatLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, GoGenTemplateParserRULE_floatLiteral)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(204)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserT__13 {
		{
			p.SetState(203)
			p.Match(GoGenTemplateParserT__13)
		}

	}
	{
		p.SetState(206)
		p.Match(GoGenTemplateParserINT)
	}
	{
		p.SetState(207)
		p.Match(GoGenTemplateParserDOT)
	}
	{
		p.SetState(208)
		p.Match(GoGenTemplateParserINT)
	}

	return localctx
}

// IStringLiteralContext is an interface to support dynamic dispatch.
type IStringLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStringLiteralContext differentiates from other interfaces.
	IsStringLiteralContext()
}

type StringLiteralContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStringLiteralContext() *StringLiteralContext {
	var p = new(StringLiteralContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_stringLiteral
	return p
}

func (*StringLiteralContext) IsStringLiteralContext() {}

func NewStringLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StringLiteralContext {
	var p = new(StringLiteralContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_stringLiteral

	return p
}

func (s *StringLiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *StringLiteralContext) STRING() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserSTRING, 0)
}

func (s *StringLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StringLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitStringLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) StringLiteral() (localctx IStringLiteralContext) {
	localctx = NewStringLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 42, GoGenTemplateParserRULE_stringLiteral)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(210)
		p.Match(GoGenTemplateParserSTRING)
	}

	return localctx
}

// ISelectorChainContext is an interface to support dynamic dispatch.
type ISelectorChainContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSelectorChainContext differentiates from other interfaces.
	IsSelectorChainContext()
}

type SelectorChainContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySelectorChainContext() *SelectorChainContext {
	var p = new(SelectorChainContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_selectorChain
	return p
}

func (*SelectorChainContext) IsSelectorChainContext() {}

func NewSelectorChainContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SelectorChainContext {
	var p = new(SelectorChainContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_selectorChain

	return p
}

func (s *SelectorChainContext) GetParser() antlr.Parser { return s.parser }

func (s *SelectorChainContext) RootSelector() IRootSelectorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRootSelectorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRootSelectorContext)
}

func (s *SelectorChainContext) Variable() IVariableContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IVariableContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IVariableContext)
}

func (s *SelectorChainContext) AllSelector() []ISelectorContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*ISelectorContext)(nil)).Elem())
	var tst = make([]ISelectorContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(ISelectorContext)
		}
	}

	return tst
}

func (s *SelectorChainContext) Selector(i int) ISelectorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISelectorContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(ISelectorContext)
}

func (s *SelectorChainContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SelectorChainContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SelectorChainContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitSelectorChain(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) SelectorChain() (localctx ISelectorChainContext) {
	localctx = NewSelectorChainContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 44, GoGenTemplateParserRULE_selectorChain)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(227)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 19, p.GetParserRuleContext()) {
	case 1:
		p.SetState(224)
		p.GetErrorHandler().Sync(p)
		switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 18, p.GetParserRuleContext()) {
		case 1:
			{
				p.SetState(212)
				p.Variable()
			}

		case 2:
			{
				p.SetState(213)
				p.Variable()
			}
			p.SetState(215)
			p.GetErrorHandler().Sync(p)
			_alt = 1
			for ok := true; ok; ok = _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
				switch _alt {
				case 1:
					{
						p.SetState(214)
						p.Selector()
					}

				default:
					panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
				}

				p.SetState(217)
				p.GetErrorHandler().Sync(p)
				_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 16, p.GetParserRuleContext())
			}

		case 3:
			p.SetState(220)
			p.GetErrorHandler().Sync(p)
			_alt = 1
			for ok := true; ok; ok = _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
				switch _alt {
				case 1:
					{
						p.SetState(219)
						p.Selector()
					}

				default:
					panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
				}

				p.SetState(222)
				p.GetErrorHandler().Sync(p)
				_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 17, p.GetParserRuleContext())
			}

		}

	case 2:
		{
			p.SetState(226)
			p.RootSelector()
		}

	}

	return localctx
}

// IRootSelectorContext is an interface to support dynamic dispatch.
type IRootSelectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsRootSelectorContext differentiates from other interfaces.
	IsRootSelectorContext()
}

type RootSelectorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootSelectorContext() *RootSelectorContext {
	var p = new(RootSelectorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_rootSelector
	return p
}

func (*RootSelectorContext) IsRootSelectorContext() {}

func NewRootSelectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootSelectorContext {
	var p = new(RootSelectorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_rootSelector

	return p
}

func (s *RootSelectorContext) GetParser() antlr.Parser { return s.parser }

func (s *RootSelectorContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *RootSelectorContext) Index() IIndexContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIndexContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIndexContext)
}

func (s *RootSelectorContext) SelectorChain() ISelectorChainContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISelectorChainContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISelectorChainContext)
}

func (s *RootSelectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootSelectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootSelectorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitRootSelector(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) RootSelector() (localctx IRootSelectorContext) {
	localctx = NewRootSelectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 46, GoGenTemplateParserRULE_rootSelector)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(229)
		p.Match(GoGenTemplateParserDOT)
	}
	p.SetState(234)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 21, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(230)
			p.Index()
		}
		p.SetState(232)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 20, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(231)
				p.SelectorChain()
			}

		}

	}

	return localctx
}

// ISelectorContext is an interface to support dynamic dispatch.
type ISelectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSelectorContext differentiates from other interfaces.
	IsSelectorContext()
}

type SelectorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySelectorContext() *SelectorContext {
	var p = new(SelectorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_selector
	return p
}

func (*SelectorContext) IsSelectorContext() {}

func NewSelectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SelectorContext {
	var p = new(SelectorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_selector

	return p
}

func (s *SelectorContext) GetParser() antlr.Parser { return s.parser }

func (s *SelectorContext) FieldSelector() IFieldSelectorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFieldSelectorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFieldSelectorContext)
}

func (s *SelectorContext) Index() IIndexContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIndexContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIndexContext)
}

func (s *SelectorContext) Call() ICallContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallContext)
}

func (s *SelectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SelectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SelectorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitSelector(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Selector() (localctx ISelectorContext) {
	localctx = NewSelectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 48, GoGenTemplateParserRULE_selector)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(239)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case GoGenTemplateParserDOT:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(236)
			p.FieldSelector()
		}

	case GoGenTemplateParserT__27:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(237)
			p.Index()
		}

	case GoGenTemplateParserT__23:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(238)
			p.Call()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IFieldSelectorContext is an interface to support dynamic dispatch.
type IFieldSelectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFieldSelectorContext differentiates from other interfaces.
	IsFieldSelectorContext()
}

type FieldSelectorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFieldSelectorContext() *FieldSelectorContext {
	var p = new(FieldSelectorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_fieldSelector
	return p
}

func (*FieldSelectorContext) IsFieldSelectorContext() {}

func NewFieldSelectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FieldSelectorContext {
	var p = new(FieldSelectorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_fieldSelector

	return p
}

func (s *FieldSelectorContext) GetParser() antlr.Parser { return s.parser }

func (s *FieldSelectorContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *FieldSelectorContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *FieldSelectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FieldSelectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FieldSelectorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitFieldSelector(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) FieldSelector() (localctx IFieldSelectorContext) {
	localctx = NewFieldSelectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 50, GoGenTemplateParserRULE_fieldSelector)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(241)
		p.Match(GoGenTemplateParserDOT)
	}
	{
		p.SetState(242)
		p.Identifier()
	}

	return localctx
}

// IIndexContext is an interface to support dynamic dispatch.
type IIndexContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIndexContext differentiates from other interfaces.
	IsIndexContext()
}

type IndexContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIndexContext() *IndexContext {
	var p = new(IndexContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_index
	return p
}

func (*IndexContext) IsIndexContext() {}

func NewIndexContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IndexContext {
	var p = new(IndexContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_index

	return p
}

func (s *IndexContext) GetParser() antlr.Parser { return s.parser }

func (s *IndexContext) INT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINT, 0)
}

func (s *IndexContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IndexContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IndexContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIndex(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Index() (localctx IIndexContext) {
	localctx = NewIndexContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 52, GoGenTemplateParserRULE_index)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(244)
		p.Match(GoGenTemplateParserT__27)
	}
	{
		p.SetState(245)
		p.Match(GoGenTemplateParserINT)
	}
	{
		p.SetState(246)
		p.Match(GoGenTemplateParserT__28)
	}

	return localctx
}

// ICallContext is an interface to support dynamic dispatch.
type ICallContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCallContext differentiates from other interfaces.
	IsCallContext()
}

type CallContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallContext() *CallContext {
	var p = new(CallContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_call
	return p
}

func (*CallContext) IsCallContext() {}

func NewCallContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallContext {
	var p = new(CallContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_call

	return p
}

func (s *CallContext) GetParser() antlr.Parser { return s.parser }
func (s *CallContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitCall(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Call() (localctx ICallContext) {
	localctx = NewCallContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 54, GoGenTemplateParserRULE_call)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(248)
		p.Match(GoGenTemplateParserT__23)
	}
	{
		p.SetState(249)
		p.Match(GoGenTemplateParserT__24)
	}

	return localctx
}

// IVariableContext is an interface to support dynamic dispatch.
type IVariableContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsVariableContext differentiates from other interfaces.
	IsVariableContext()
}

type VariableContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyVariableContext() *VariableContext {
	var p = new(VariableContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_variable
	return p
}

func (*VariableContext) IsVariableContext() {}

func NewVariableContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *VariableContext {
	var p = new(VariableContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_variable

	return p
}

func (s *VariableContext) GetParser() antlr.Parser { return s.parser }

func (s *VariableContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *VariableContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *VariableContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *VariableContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitVariable(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Variable() (localctx IVariableContext) {
	localctx = NewVariableContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 56, GoGenTemplateParserRULE_variable)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(251)
		p.Identifier()
	}

	return localctx
}

func (p *GoGenTemplateParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 14:
		var t *ExpressionContext = nil
		if localctx != nil {
			t = localctx.(*ExpressionContext)
		}
		return p.Expression_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *GoGenTemplateParser) Expression_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 6)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 5)

	case 2:
		return p.Precpred(p.GetParserRuleContext(), 4)

	case 3:
		return p.Precpred(p.GetParserRuleContext(), 3)

	case 4:
		return p.Precpred(p.GetParserRuleContext(), 2)

	case 5:
		return p.Precpred(p.GetParserRuleContext(), 1)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
