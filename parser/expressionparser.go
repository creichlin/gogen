package parser

import (
	"fmt"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"github.com/pkg/errors"
	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/gogen/parser/internal/ggt"
	"gitlab.com/creichlin/gogen/util"
)

func createParserFor(i string, l util.Location) (*ggt.GoGenTemplateParser, *TemplateVisitor) {
	input := antlr.NewInputStream(i)
	lexer := ggt.NewGoGenTemplateLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := ggt.NewGoGenTemplateParser(stream)
	p.BuildParseTrees = true

	visi := &TemplateVisitor{loc: l, errors: gogen.NewErrors()}
	p.RemoveErrorListeners()
	p.AddErrorListener(visi)
	return p, visi
}

func parseVisitDefaultParameters(i string, l util.Location) ([]*gogen.Parameter, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.VisitDefaultParameters().Accept(visitor)
	return result.([]*gogen.Parameter), visitor.errors
}

func parseVisit(i string, l util.Location) (*gogen.VisitNode, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.View().Accept(visitor)
	return result.(*gogen.VisitNode), visitor.errors
}

func parseEcho(i string, l util.Location) (*gogen.EchoNode, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.Echo().Accept(visitor)
	return result.(*gogen.EchoNode), visitor.errors
}

func parseFor(i string, l util.Location) (*gogen.ForNode, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.ForBlock().Accept(visitor)
	return result.(*gogen.ForNode), visitor.errors
}

func parseIndent(i string, l util.Location) (*gogen.IndentNode, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.IndentBlock().Accept(visitor)
	return result.(*gogen.IndentNode), visitor.errors
}

func parseIf(i string, l util.Location) (*gogen.IfNode, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.IfBlock().Accept(visitor)
	return result.(*gogen.IfNode), visitor.errors
}

func parseElse(i string, l util.Location) (gogen.Silent, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.ElseBlock().Accept(visitor)
	return result.(gogen.Silent), visitor.errors
}

func parseEnd(i string, l util.Location) (gogen.Silent, *gogen.Errors) {
	parser, visitor := createParserFor(i, l)
	result := parser.End().Accept(visitor)
	return result.(gogen.Silent), visitor.errors
}

type TemplateVisitor struct {
	*ggt.BaseGoGenTemplateVisitor
	loc    util.Location
	errors *gogen.Errors
}

func (v *TemplateVisitor) VisitEcho(ctx *ggt.EchoContext) interface{} {
	return &gogen.EchoNode{
		BaseNode:   v.createBaseNodeFromRule(ctx),
		Expression: ctx.Expression().Accept(v).(gogen.Expression),
		Silent:     parseSilent(ctx.Opn(), ctx.Cls()),
	}
}

func (v *TemplateVisitor) VisitView(ctx *ggt.ViewContext) interface{} {
	vn := &gogen.VisitNode{
		BaseNode:   v.createBaseNodeFromRule(ctx),
		Expression: ctx.Expression().Accept(v).(gogen.Expression),
		Silent:     parseSilent(ctx.Opn(), ctx.Cls()),
	}
	if ctx.GetPkg() != nil {
		vn.Package = ctx.GetPkg().GetText()
	}
	if ctx.GetName() != nil {
		vn.Visitor = ctx.GetName().GetText()
	}

	for _, param := range ctx.AllViewParam() {
		vn.Parameters = append(vn.Parameters, param.Accept(v).(*gogen.Parameter))
	}

	return vn
}

func (v *TemplateVisitor) VisitViewParam(ctx *ggt.ViewParamContext) interface{} {
	param := &gogen.Parameter{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.IDENTIFIER().GetText(),
	}

	expr := ctx.Expression()
	if expr != nil {
		param.Value = expr.Accept(v).(gogen.Expression)
	}

	return param
}

func (v *TemplateVisitor) VisitVisitDefaultParameters(ctx *ggt.VisitDefaultParametersContext) interface{} {
	params := []*gogen.Parameter{}

	for _, c := range ctx.AllDefaultParam() {
		params = append(params, c.Accept(v).(*gogen.Parameter))
	}

	return params
}

func (v *TemplateVisitor) VisitDefaultParam(ctx *ggt.DefaultParamContext) interface{} {
	param := &gogen.Parameter{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.IDENTIFIER().GetText(),
	}

	expr := ctx.Literal()
	if expr != nil {
		param.Value = expr.Accept(v).(gogen.Expression)
	}

	return param
}

func (v *TemplateVisitor) VisitForBlock(ctx *ggt.ForBlockContext) interface{} {
	loop := &gogen.ForNode{
		BaseNode:  v.createBaseNodeFromRule(ctx),
		First:     len(ctx.AllFIRST()) > 0,
		Last:      len(ctx.AllLAST()) > 0,
		Even:      len(ctx.AllEVEN()) > 0,
		SilentFor: parseSilent(ctx.Opn(), ctx.Cls()),
	}

	if ctx.GetValue() == nil {
		loop.Value = ctx.GetKey().GetText()
	} else {
		loop.Key = ctx.GetKey().GetText()
		loop.Value = ctx.GetValue().GetText()
	}

	if ctx.Expression() != nil {
		loop.Expression = ctx.Expression().Accept(v).(gogen.Expression)
	}

	return loop
}

func (v *TemplateVisitor) VisitIndentBlock(ctx *ggt.IndentBlockContext) interface{} {
	indent := &gogen.IndentNode{
		BaseNode:     v.createBaseNodeFromRule(ctx),
		SilentIndent: parseSilent(ctx.Opn(), ctx.Cls()),
	}
	if ctx.Expression() != nil {
		indent.Expression = ctx.Expression().Accept(v).(gogen.Expression)
	}
	return indent
}

func (v *TemplateVisitor) VisitIfBlock(ctx *ggt.IfBlockContext) interface{} {
	ifelse := &gogen.IfNode{
		BaseNode:   v.createBaseNodeFromRule(ctx),
		Expression: ctx.Expression().Accept(v).(gogen.Expression),
		SilentIf:   parseSilent(ctx.Opn(), ctx.Cls()),
	}
	return ifelse
}

func (v *TemplateVisitor) VisitElseBlock(ctx *ggt.ElseBlockContext) interface{} {
	return parseSilent(ctx.Opn(), ctx.Cls())
}

func (v *TemplateVisitor) VisitEnd(ctx *ggt.EndContext) interface{} {
	return parseSilent(ctx.Opn(), ctx.Cls())
}

func (v *TemplateVisitor) VisitRootExpression(ctx *ggt.RootExpressionContext) interface{} {
	return ctx.Expression().Accept(v)
}

func (v *TemplateVisitor) VisitExpression(ctx *ggt.ExpressionContext) interface{} {
	if ctx.Operand() != nil {
		return ctx.Operand().Accept(v)
	}
	if ctx.UnaryExpression() != nil {
		return ctx.UnaryExpression().Accept(v)
	}

	all := ctx.AllExpression()
	if len(all) == 2 {
		return &gogen.BinaryOperation{
			BaseNode: v.createBaseNodeFromRule(ctx),
			Left:     all[0].Accept(v).(gogen.Expression),
			Right:    all[1].Accept(v).(gogen.Expression),
			Operator: ctx.GetOperator().GetText(),
		}
	}

	if ctx.GetFilter() != nil {
		return &gogen.Filter{
			BaseNode:   v.createBaseNode(ctx.GetFilter()),
			Name:       ctx.GetFilter().GetText(),
			Expression: all[0].Accept(v).(gogen.Expression),
		}
	}

	panic(fmt.Errorf("invalid expression %v", ctx.GetText()))
}

func (v *TemplateVisitor) VisitUnaryExpression(ctx *ggt.UnaryExpressionContext) interface{} {
	if ctx.NOT() != nil {
		return &gogen.NotOperation{
			BaseNode:   v.createBaseNodeFromRule(ctx),
			Expression: ctx.Expression().Accept(v).(gogen.Expression),
		}
	}
	panic(fmt.Errorf("unexpected unary expression %v", ctx.GetText()))
}

func (v *TemplateVisitor) VisitOperand(ctx *ggt.OperandContext) interface{} {
	if ctx.Literal() != nil {
		return ctx.Literal().Accept(v)
	}
	if ctx.SelectorChain() != nil {
		return ctx.SelectorChain().Accept(v)
	}
	if ctx.Expression() != nil {
		return ctx.Expression().Accept(v)
	}
	panic("invalid operand")
}

func (v *TemplateVisitor) VisitLiteral(ctx *ggt.LiteralContext) interface{} {
	if ctx.BoolLiteral() != nil {
		return ctx.BoolLiteral().Accept(v)
	}
	if ctx.IntLiteral() != nil {
		return ctx.IntLiteral().Accept(v)
	}
	if ctx.FloatLiteral() != nil {
		return ctx.FloatLiteral().Accept(v)
	}
	if ctx.StringLiteral() != nil {
		return ctx.StringLiteral().Accept(v)
	}
	panic("unknown literal")
}

func (v *TemplateVisitor) VisitBoolLiteral(ctx *ggt.BoolLiteralContext) interface{} {
	return &gogen.BoolLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: ctx.GetText() == "true"}
}

func (v *TemplateVisitor) VisitIntLiteral(ctx *ggt.IntLiteralContext) interface{} {
	i, err := strconv.ParseInt(ctx.GetText(), 10, 64)
	if err != nil {
		panic(errors.WithStack(err))
	}
	return &gogen.IntLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: i}
}

func (v *TemplateVisitor) VisitFloatLiteral(ctx *ggt.FloatLiteralContext) interface{} {
	i, err := strconv.ParseFloat(ctx.GetText(), 64)
	if err != nil {
		panic(errors.WithStack(err))
	}
	return &gogen.FloatLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: i}
}
func (v *TemplateVisitor) VisitStringLiteral(ctx *ggt.StringLiteralContext) interface{} {
	str := ctx.GetText()
	str = str[1 : len(str)-1]
	return &gogen.StringLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: str}
}

func (v *TemplateVisitor) VisitSelectorChain(ctx *ggt.SelectorChainContext) interface{} {
	sc := &gogen.SelectorChain{
		BaseNode: v.createBaseNodeFromRule(ctx),
	}

	if ctx.Variable() != nil {
		sc.Selectors = append(sc.Selectors, ctx.Variable().Accept(v).(*gogen.VariableSelector))
	}

	for _, s := range ctx.AllSelector() {
		sc.Selectors = append(sc.Selectors, s.Accept(v).(gogen.Selector))
	}

	return sc
}

func (v *TemplateVisitor) VisitSelector(ctx *ggt.SelectorContext) interface{} {
	if ctx.FieldSelector() != nil {
		return ctx.FieldSelector().Accept(v)
	}
	if ctx.Call() != nil {
		return ctx.Call().Accept(v)
	}
	panic("invalid selector")
}

func (v *TemplateVisitor) VisitVariable(ctx *ggt.VariableContext) interface{} {
	return &gogen.VariableSelector{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.Identifier().GetText(),
	}
}

func (v *TemplateVisitor) VisitFieldSelector(ctx *ggt.FieldSelectorContext) interface{} {
	return &gogen.FieldSelector{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.GetText()[1:],
	}
}

func (v *TemplateVisitor) VisitCall(ctx *ggt.CallContext) interface{} {
	return &gogen.CallSelector{
		BaseNode: v.createBaseNodeFromRule(ctx),
	}
}
