grammar GoGenTemplate;

FOR: 'for';
IN: 'in';
END: 'end';
IF: 'if';
INDENT: 'indent';
ELSE: 'else';
FIRST: 'first';
LAST: 'last';
EVEN: 'even';

STRING: '"' .*? '"';
IDENTIFIER: [A-Za-z][a-zA-Z0-9]*;
INT: [0-9]+;
DOT: '.';
NOT: '!';

WS: [ \r\n\t]+ -> skip;

identifier:
	IDENTIFIER
	| FOR
	| IN
	| END
	| IF
	| ELSE
	| FIRST
	| LAST
	| EVEN
	| INDENT;

opn: '{-' | '{';
cls: '-}' | '}';

end: opn END cls;

forBlock:
	opn FOR key = identifier (',' value = identifier)? (
		',' (FIRST | LAST | EVEN)
	)* IN expression cls;

ifBlock: opn IF expression cls;
elseBlock: opn ELSE cls;

indentBlock: opn INDENT expression? cls;

echo: '$' opn expression cls;

view:
	'@' opn ((pkg = IDENTIFIER '.')? name = IDENTIFIER ':')? expression (
		viewParam (',' viewParam)*
	)? cls;

viewParam: IDENTIFIER '=' expression;

visitDefaultParameters: (defaultParam (',' defaultParam)*)?;

defaultParam: IDENTIFIER '=' literal;

rootExpression: expression EOF;

expression:
	operand
	| unaryExpression
	| expression operator = ('*' | '/' | '%') expression
	| expression operator = ('+' | '-') expression
	| expression operator = (
		'=='
		| '!='
		| '<'
		| '<='
		| '>'
		| '>='
	) expression
	| expression operator = '&&' expression
	| expression operator = '||' expression
	| expression '|' filter = IDENTIFIER;

operand: literal | selectorChain | '(' expression ')';

unaryExpression: NOT expression;

literal:
	boolLiteral
	| intLiteral
	| floatLiteral
	| stringLiteral;

boolLiteral: 'true' | 'false';
intLiteral: '-'? INT;
floatLiteral: '-'? INT '.' INT;
stringLiteral: STRING;

selectorChain: (
		(variable | variable selector+ | selector+)
		| rootSelector
	);

// root selector to not allow ..Field but . or .[3].Field
rootSelector: DOT (index selectorChain?)?;

selector: fieldSelector | index | call;

fieldSelector: DOT identifier;

index: '[' INT ']';

call: '(' ')';

variable: identifier;