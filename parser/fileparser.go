package parser

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/gogen/util"
)

func parseFile(code, source string, imports func(string) (string, string, error)) (*gogen.AST, *gogen.Errors) {
	errors := gogen.NewErrors()
	ast := &gogen.AST{
		Imports: map[string]*gogen.AST{},
		BaseNode: &gogen.BaseNode{
			Location: util.Loc(source, 0, 0),
		},
	}

	lines := strings.Split(code, "\n")

	var visitor *gogen.Visitor
	var template *gogen.Template
out:
	for i := 0; i < len(lines); i++ {
		if is, name := isVisitorLine(lines[i]); is {
			visitor = &gogen.Visitor{Name: name}
			ast.Visitors = append(ast.Visitors, visitor)
			continue
		}
		if is, name := isImportLine(lines[i]); is {
			ast.Imports[name] = nil
			continue
		}
		if is, tp, params := isTypeLine(lines[i]); is {
			if visitor == nil {
				errors.Push(fmt.Errorf("%v: type statement must always be after a visitor statement", (i + 1)))
				continue
			}

			var defaultParameters []*gogen.Parameter
			if len(params) > 0 {
				params = params[1 : len(params)-1]
				var errs *gogen.Errors
				defaultParameters, errs = parseVisitDefaultParameters(params, util.Loc(source, i, 0))
				if errs != nil {
					errors.PushAll(errs)
				}
			}

			template = &gogen.Template{
				Name:              tp,
				DefaultParameters: defaultParameters,
			}

			visitor.Templates = append(visitor.Templates, template)
			code := []string{}
			line := i
			for i++; ; i++ {
				if i < len(lines) && isCodeLine(lines[i]) {
					code = append(code, lines[i])
				} else {
					unPadded, column, err := removeSpaceBlock(strings.Join(code, "\n"))
					if err != nil {
						errors.Push(err)
					}
					var errs *gogen.Errors
					template.Nodes, errs = parseControl(unPadded, util.Loc(source, line+1, column))
					if errs != nil {
						errors.PushAll(errs)
					}
					i-- // we have to try this line again
					continue out
				}
			}
		}
		if isComment(lines[i]) {
			continue
		}
		if isEmpty(lines[i]) {
			continue
		}
		fmt.Println("UNKNOWN LINE", lines[i])
	}

	// resolve imports

	for k := range ast.Imports {
		code, source, err := imports(k)
		if err != nil {
			errors.Push(err)
			continue
		}

		iAst, errs := ParseDetailed(code, source, imports)
		if errs.Has() {
			errors.PushAll(errs)
			continue
		}
		ast.Imports[k] = iAst
	}

	return ast, errors
}

var visitorLine = regexp.MustCompile(`^([A-Za-z-0-9]+)[ \t]+visitor:[ \t]*`)

func isVisitorLine(l string) (bool, string) {
	matches := visitorLine.FindStringSubmatch(l)
	if matches == nil {
		return false, ""
	}
	return true, matches[1]
}

var importLine = regexp.MustCompile(`^import ([a-z-0-9]+)[ \t]*`)

func isImportLine(l string) (bool, string) {
	matches := importLine.FindStringSubmatch(l)
	if matches == nil {
		return false, ""
	}
	return true, matches[1]
}

var typeLine = regexp.MustCompile(`^(((\[\])?[A-Za-z-0-9]+(.[A-Za-z-0-9]+)?)|<nil>|\*)[ \t]+type(\([^)]*\))?:[ \t]*$`)

func isTypeLine(l string) (bool, string, string) {
	matches := typeLine.FindStringSubmatch(l)
	if matches == nil {
		return false, "", ""
	}
	return true, matches[1], matches[5]
}

func isCodeLine(l string) bool {
	if len(l) == 0 {
		return true
	}
	if l[0:1] == " " {
		return true
	}
	if l[0:1] == "\t" {
		return true
	}
	return false
}

func isComment(l string) bool {
	return strings.HasPrefix(strings.Trim(l, " \t"), "//")
}

func isEmpty(l string) bool {
	return strings.Trim(l, " \t") == ""
}

var countSpaces = regexp.MustCompile("^ *")
var countTabs = regexp.MustCompile("^\t*")

const invalid = 10000

func removeSpaceBlock(i string) (string, int, error) {
	lines := strings.Split(i, "\n")

	spaces := invalid
	tabs := invalid

	for _, l := range lines {
		ls := strings.TrimRight(l, " \t")
		if len(ls) > 0 { // we check only lines with some non space chars

			sp := countSpaces.FindString(ls)
			tb := countTabs.FindString(ls)

			if len(sp) != 0 && len(sp) < spaces {
				spaces = len(sp)
			}
			if len(tb) != 0 && len(tb) < tabs {
				tabs = len(tb)
			}
		}
	}

	prefix := ""
	columns := 0
	if spaces != invalid && spaces > 0 {
		prefix = strings.Repeat(" ", spaces)
		columns = spaces
	}
	if tabs != invalid && tabs > 0 {
		prefix = strings.Repeat("\t", tabs)
		columns = tabs
	}

	for i := range lines {
		lines[i] = strings.TrimPrefix(lines[i], prefix)
	}

	return strings.Join(lines, "\n"), columns, nil
}
