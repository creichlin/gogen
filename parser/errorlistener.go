package parser

import (
	"errors"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/creichlin/gogen/util"
)

func (v *TemplateVisitor) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{}, line, column int, msg string, e antlr.RecognitionException) {
	antLoc := util.Loc("", line-1, column) // line is 1 base, column is zero based
	v.errors.PushLocation(v.loc.Add(antLoc), errors.New(msg))
}

func (v *TemplateVisitor) ReportAmbiguity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, exact bool, ambigAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	// fmt.Println("ReportAmbiguity")
}

func (v *TemplateVisitor) ReportAttemptingFullContext(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, conflictingAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	// fmt.Println("ReportAttemptingFullContext")
}

func (v *TemplateVisitor) ReportContextSensitivity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex, prediction int, configs antlr.ATNConfigSet) {
	// fmt.Println("ReportContextSensitivity")
}
