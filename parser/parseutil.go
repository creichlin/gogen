package parser

import (
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/gogen/parser/internal/ggt"
	"gitlab.com/creichlin/gogen/util"
)

func parseSilent(opn ggt.IOpnContext, cls ggt.IClsContext) gogen.Silent {
	o := ""
	if opn != nil {
		o = opn.GetText()
	}
	c := ""
	if cls != nil {
		c = cls.GetText()
	}

	return gogen.Silent{
		Before: strings.Contains(o, "-"),
		After:  strings.Contains(c, "-"),
	}
}

func (v *TemplateVisitor) createBaseNode(start antlr.Token) *gogen.BaseNode {
	return &gogen.BaseNode{
		Location: v.loc.Add(util.Loc("",
			start.GetLine()-1, // line is one based in antlr, so -1
			start.GetColumn(), // column is 0 based
		)),
	}
}

func (v *TemplateVisitor) createBaseNodeFromRule(prc antlr.ParserRuleContext) *gogen.BaseNode {
	return v.createBaseNode(prc.GetStart())
}
