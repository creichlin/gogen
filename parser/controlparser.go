package parser

import (
	"fmt"

	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/gogen/util"
)

type controlParser struct {
	lexer   controlLexer
	current symbol
	errors  *gogen.Errors
	From    util.Location
}

func parseControl(s string, from util.Location) ([]gogen.Node, *gogen.Errors) {
	cp := &controlParser{
		lexer:  controlLexer{source: []rune(s)},
		errors: gogen.NewErrors(),
		From:   from,
	}

	cp.next()
	nodes := cp.parseBlock()
	return nodes, cp.errors
}

func (cp *controlParser) parseBlock() []gogen.Node {
	nodes := []gogen.Node{}
	for {
		switch cp.current.t {
		case Text:
			nodes = append(nodes, cp.parseText())
		case Print:
			nodes = append(nodes, cp.parseEcho())
		case Visit:
			nodes = append(nodes, cp.parseVisit())
		case If:
			nodes = append(nodes, cp.parseIf())
		case For:
			nodes = append(nodes, cp.parseFor())
		case Indent:
			nodes = append(nodes, cp.parseIndent())
		case End, Else, ElseIf, EOF:
			// go back one level higher while keeping the current symbol
			return nodes
		default:
			l := cp.loc()
			cp.errors.PushLocation(l, fmt.Errorf("unexpected token %v", cp.current.t))
		}
	}
}

func (cp *controlParser) parseText() *gogen.TextNode {
	tn := &gogen.TextNode{
		Text:     cp.str(),
		BaseNode: &gogen.BaseNode{Location: cp.loc()},
	}
	cp.next()
	return tn
}

func (cp *controlParser) parseEcho() *gogen.EchoNode {
	en, errs := parseEcho(cp.str(), cp.loc())
	cp.errors.PushAll(errs)
	cp.next()
	return en
}

func (cp *controlParser) parseVisit() *gogen.VisitNode {
	vn, errs := parseVisit(cp.str(), cp.loc())
	cp.errors.PushAll(errs)
	cp.next()
	return vn
}

func (cp *controlParser) parseIf() *gogen.IfNode {
	i, errs := parseIf(cp.str(), cp.loc())
	cp.errors.PushAll(errs)
	cp.next()
	i.IfNodes = cp.parseBlock()
	if cp.is(Else) {
		elseb, errs := parseElse(cp.str(), cp.loc())
		cp.errors.PushAll(errs)
		i.SilentElse = elseb
		cp.next()
		i.ElseNodes = cp.parseBlock()
	}
	if cp.must(End) {
		end, errs := parseEnd(cp.str(), cp.loc())
		cp.errors.PushAll(errs)
		cp.next()
		i.SilentEnd = end
	}
	return i
}

func (cp *controlParser) parseFor() *gogen.ForNode {
	fn, errs := parseFor(cp.str(), cp.loc())
	cp.errors.PushAll(errs)

	cp.next()
	fn.Nodes = cp.parseBlock()
	if cp.must(End) {
		end, errs := parseEnd(cp.str(), cp.loc())
		cp.errors.PushAll(errs)
		fn.SilentEnd = end
		cp.next()
	}

	return fn
}

func (cp *controlParser) parseIndent() *gogen.IndentNode {
	fn, errs := parseIndent(cp.str(), cp.loc())
	cp.errors.PushAll(errs)

	cp.next()
	fn.Nodes = cp.parseBlock()
	if cp.must(End) {
		end, errs := parseEnd(cp.str(), cp.loc())
		cp.errors.PushAll(errs)
		fn.SilentEnd = end
		cp.next()
	}

	return fn
}

// next reads next symbol and sets it as current
func (cp *controlParser) next() {
	cp.current = cp.lexer.next()
}

// str returns the input string of the current symbol
func (cp *controlParser) str() string {
	return cp.lexer.str(cp.current)
}

// loc returns a location object of the current symbol
func (cp *controlParser) loc() util.Location {
	line, column := cp.lexer.position(cp.current)
	return cp.From.Add(util.Loc("", line, column))
}

// checks if current symbol is of given type
func (cp *controlParser) is(s symbolType) bool {
	return cp.current.t == s
}

// checks if current symbol is of given type and if not it will add an error
func (cp *controlParser) must(s symbolType) bool {
	if !cp.is(s) {
		cp.errors.PushLocation(cp.loc(), fmt.Errorf("expected %v but found %v", s, cp.current.t))
		return false
	}
	return true
}
