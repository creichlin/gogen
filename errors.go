package gogen

import (
	"fmt"
	"strings"

	"gitlab.com/creichlin/gogen/util"
)

type Errors struct {
	errs []error
}

type Error struct {
	err      error
	location util.Location
}

func (e *Error) Error() string {
	return fmt.Sprintf("%v line %v col %v: %v", e.location.Source, e.location.Line+1, e.location.Column, e.err.Error())
}

func (e *Error) Location() (string, int, int) {
	return e.location.Source, e.location.Line, e.location.Column
}

func NewErrors() *Errors {
	return &Errors{}
}

func (e *Errors) Error() string {
	messages := []string{}
	for _, e := range e.errs {
		messages = append(messages, e.Error())
	}
	return strings.Join(messages, "\n")
}

func NewError(l util.Location, err error) error {
	return &Error{
		err:      err,
		location: l,
	}
}

func (e *Errors) Push(err error) {
	if err != nil {
		e.errs = append(e.errs, err)
	}
}

func (e *Errors) PushLocation(l util.Location, err error) {
	if err != nil {
		e.errs = append(e.errs, &Error{location: l, err: err})
	}
}

func (e *Errors) PushAll(errs *Errors) {
	e.errs = append(e.errs, errs.errs...)
}

func (e *Errors) Get() []error {
	return e.errs
}

func (e *Errors) Has() bool {
	return len(e.errs) > 0
}
