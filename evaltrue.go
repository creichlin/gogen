package gogen

import (
	"reflect"
)

func evalsTrue(val interface{}) bool {
	if val == nil {
		return false
	}
	rv := reflect.ValueOf(val)
	if rv.Kind() == reflect.Ptr && rv.IsNil() {
		return false
	}
	switch v := val.(type) {
	case bool:
		return v
	case string:
		return v != ""
	case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
		return rv.Int() != 0
	case float32, float64:
		return rv.Float() != 0
	}
	return true
}
