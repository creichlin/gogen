package gogen

import (
	"fmt"
	"reflect"
	"strings"
)

type Option interface {
	apply(*rt) error
}

type filterOption struct {
	name   string
	filter interface{}
}

func (f *filterOption) apply(rt *rt) error {
	return rt.filters.add(f.name, f.filter)
}

func FilterOption(name string, f interface{}) Option {
	return &filterOption{
		name:   name,
		filter: f,
	}
}

type varOption struct {
	name  string
	value interface{}
}

func (v *varOption) apply(rt *rt) error {
	rt.pushVar(v.name, v.value)
	return nil
}

func WithVar(name string, f interface{}) Option {
	return &varOption{
		name:  name,
		value: f,
	}
}

func Execute(ast *AST, model interface{}, options ...Option) (string, error) {
	r, err := ExecuteDetailed(ast, model, options...)
	if err.Has() {
		return "", err
	}
	return r, nil
}

func ExecuteDetailed(ast *AST, model interface{}, options ...Option) (string, *Errors) {
	errs := NewErrors()
	output := ""
	rt := &rt{
		ast:     ast,
		visitor: []string{"main"},
		errors:  errs,
		output:  &output,
		filters: newFilters(),
	}

	opts := []Option{}
	opts = append(opts, defaultFilters...)
	opts = append(opts, options...)

	for _, o := range opts {
		err := o.apply(rt)
		if err != nil {
			errs.Push(err)
			return "", errs
		}
	}

	err := rt.render(model)

	if err != nil {
		errs.PushLocation(ast.Location, err)
	}
	return output, errs
}

type rt struct {
	ast       *AST
	visitor   []string
	variables []variable
	indent    []int
	output    *string
	errors    *Errors
	filters   *filters
}

type variable struct {
	name  string
	value interface{}
}

func (r *rt) pushVisitor(visitor string) {
	r.visitor = append(r.visitor, visitor)
}

func (r *rt) popVisitor() {
	r.visitor = r.visitor[:len(r.visitor)-1]
}

func (r *rt) getVisitor() string {
	return r.visitor[len(r.visitor)-1]
}

func (r *rt) pushVar(name string, value interface{}) {
	r.variables = append(r.variables, variable{
		name:  name,
		value: value,
	})
}

func (r *rt) popVar() {
	r.variables = r.variables[:len(r.variables)-1]
}

func (r *rt) currentIndent() int {
	l := strings.LastIndex(*r.output, "\n")
	c := len(*r.output)
	if l > -1 {
		c -= l + 1
	}
	return c
}

func (r *rt) pushCurrentIndent() {
	r.pushIndent(r.currentIndent())
}

func (r *rt) pushIndent(c int) {
	r.indent = append(r.indent, c)
}

func (r *rt) popIndent() {
	r.indent = r.indent[:len(r.indent)-1]
}

func (r *rt) getIndent() int {
	if len(r.indent) > 0 {
		return r.indent[len(r.indent)-1]
	}
	return 0
}

func (r *rt) append(o string) {
	lines := strings.Split(o, "\n")

	for i, line := range lines {
		toIndent := r.getIndent() - r.currentIndent()
		if toIndent > 0 {
			*r.output += strings.Repeat(" ", toIndent)
		}

		*r.output += line
		if i < len(lines)-1 {
			*r.output += "\n"
		}
	}
}

// getVar finds the variable by name, searching from top of stack
// so deeper scopes can overwrite upper scopes vars (might be
// better to not allow overriding in the first place)
func (r *rt) getVar(name string) (interface{}, error) {

	for i := len(r.variables) - 1; i >= 0; i-- {
		if r.variables[i].name == name {
			return r.variables[i].value, nil
		}
	}
	return nil, fmt.Errorf("variable '%v' does not exist", name)
}

func (r *rt) evalSelector(sel Selector, model interface{}) (interface{}, error) {
	switch selector := sel.(type) {
	case *FieldSelector:
		m := reflect.ValueOf(model)
		if m.Kind() == reflect.Ptr && m.IsNil() {
			return nil, NewError(sel.Start(), fmt.Errorf("can not access field '%v' on nil value", selector.Name))
		}
		for m.Kind() == reflect.Ptr {
			m = m.Elem()
		}

		kind := m.Kind()
		f := reflect.Value{}
		if kind == reflect.Struct {
			f = m.FieldByName(selector.Name)
		}
		if !f.IsValid() {
			// check if it's a method
			f = m.MethodByName(selector.Name)
			if !f.IsValid() {
				// if it's a method with pointer receiver
				if m.CanAddr() {
					f = m.Addr().MethodByName(selector.Name)
				}
			}
			if !f.IsValid() {
				return nil, NewError(selector.Start(), fmt.Errorf("invalid field or method '%v' on type '%T'", selector.Name, m.Interface()))
			}

			ret := f.Call([]reflect.Value{})
			if len(ret) != 1 {
				return nil, fmt.Errorf("can only call funcs with 1 return argument %T", model)
			}
			return ret[0].Interface(), nil
		}

		return f.Interface(), nil

	case *VariableSelector:
		val, err := r.getVar(selector.Name)
		if err != nil {
			return val, NewError(selector.Start(), err)
		}
		return val, nil
	case *CallSelector:
		f := reflect.ValueOf(model)
		if f.Kind() != reflect.Func {
			return nil, fmt.Errorf("try to call a non function %T", model)
		}
		ret := f.Call([]reflect.Value{})
		if len(ret) != 1 {
			return nil, fmt.Errorf("can only call funcs with 1 return argument %T", model)
		}
		return ret[0].Interface(), nil
	}
	panic(fmt.Errorf("unknown selector %T", sel))
}

func (r *rt) evalSelectorChain(sels []Selector, model interface{}) (interface{}, error) {
	if len(sels) == 0 {
		return model, nil
	}

	sel := sels[0]
	val, err := r.evalSelector(sel, model)
	if err != nil {
		return nil, err
	}

	return r.evalSelectorChain(sels[1:], val)

}

func (r *rt) evalExpression(ex Expression, model interface{}) (interface{}, error) {
	switch x := ex.(type) {
	case *SelectorChain:
		return r.evalSelectorChain(x.Selectors, model)
	case *BoolLiteral:
		return x.Value, nil
	case *IntLiteral:
		return x.Value, nil
	case *FloatLiteral:
		return x.Value, nil
	case *StringLiteral:
		return x.Value, nil
	case *Filter:
		left, err := r.evalExpression(x.Expression, model)
		if err != nil {
			return nil, err
		}

		// builtins
		if x.Name == "type" {
			return fmt.Sprintf("%T", left), nil
		}

		if x.Name == "len" {
			val := reflect.ValueOf(left)
			switch val.Kind() {
			case reflect.Slice, reflect.Array, reflect.Chan, reflect.Map, reflect.String:
				return val.Len(), nil
			}
		}

		ret, err, found := r.filters.apply(x.Name, left)
		if found {
			if err != nil {
				return ret, NewError(ex.Start(), err)
			}
			return ret, err
		}

		return nil, NewError(ex.Start(), fmt.Errorf("can not apply filter '%v' on type %T", x.Name, left))

	case *BinaryOperation:
		left, err := r.evalExpression(x.Left, model)
		if err != nil {
			return nil, err
		}
		right, err := r.evalExpression(x.Right, model)
		if err != nil {
			return nil, err
		}
		return evaluateBinaryOperation(left, x.Operator, right, x.Location)
	case *NotOperation:
		val, err := r.evalExpression(x.Expression, model)
		if err != nil {
			return nil, err
		}
		b, isbool := val.(bool)
		if isbool {
			return !b, nil
		}
		return false, nil
	}

	panic(fmt.Errorf("unknown expression type %T", ex))
}

func (r *rt) renderEcho(n *EchoNode, model interface{}) {
	val, err := r.evalExpression(n.Expression, model)
	if err != nil {
		r.errors.Push(err)
	}
	r.append(fmt.Sprint(val))
}

func (r *rt) renderVisit(n *VisitNode, model interface{}) {
	val, err := r.evalExpression(n.Expression, model)
	if err != nil {
		r.errors.Push(err)
		return
	}

	// evaluate param expressions
	paramValues := make([]interface{}, len(n.Parameters))
	for i, param := range n.Parameters {
		val, err := r.evalExpression(param.Value, model)
		if err != nil {
			r.errors.Push(err)
		}
		paramValues[i] = val
	}

	if n.Package != "" {
		r.renderImportVisit(n, model, val, paramValues)
		return
	}

	r.renderLocalVisit(n, model, val, paramValues)
}

func (r *rt) renderImportVisit(n *VisitNode, model interface{}, val interface{}, paramValues []interface{}) {
	ast, has := r.ast.Imports[n.Package]
	if !has {
		r.errors.Push(NewError(n.Start(), fmt.Errorf("package %v not found", n.Package)))
		return
	}

	sub := &rt{
		ast:     ast,
		visitor: []string{n.Visitor},
		errors:  r.errors,
		output:  r.output,
		filters: r.filters,
	}

	sub.renderLocalVisit(n, model, val, paramValues)
}

func (r *rt) renderLocalVisit(n *VisitNode, model interface{}, val interface{}, paramValues []interface{}) {
	if n.Visitor != "" {
		r.pushVisitor(n.Visitor)
		defer r.popVisitor()
	}

	tpl, err := r.getTemplate(val)
	if err != nil {
		r.errors.Push(NewError(n.Start(), err))
		return
	}

	// set default parameters
	for _, param := range tpl.DefaultParameters {
		// expression can only be a literal so there is no model needed
		val, err := r.evalExpression(param.Value, nil)
		if err != nil {
			r.errors.Push(err)
		}
		r.pushVar(param.Name, val)
		defer r.popVar()
	}

	// overwrite with actual parameters when set
	for i, param := range n.Parameters {
		r.pushVar(param.Name, paramValues[i])
		defer r.popVar()
	}

	err = r.render(val)
	if err != nil {
		r.errors.Push(NewError(n.Start(), err))
	}
}

func (r *rt) renderIndent(n *IndentNode, model interface{}) {
	if n.Expression == nil {
		r.pushCurrentIndent()
		defer r.popIndent()

	} else {
		val, err := r.evalExpression(n.Expression, model)
		if err != nil {
			r.errors.Push(err)
		}
		valInt, isInt := val.(int64)
		if !isInt {
			r.errors.Push(fmt.Errorf("indent expression must return an int but it returned %T", val))
		}

		r.pushIndent(int(valInt))
		defer r.popIndent()
	}

	for _, n := range n.Nodes {
		r.renderNode(n, model)
	}
}

func (r *rt) renderIf(n *IfNode, model interface{}) {
	val, err := r.evalExpression(n.Expression, model)
	if err != nil {
		r.errors.Push(err)
		return
	}

	var nodes []Node
	if evalsTrue(val) {
		nodes = n.IfNodes
	} else {
		nodes = n.ElseNodes
	}

	for _, n := range nodes {
		r.renderNode(n, model)
	}
}

func (r *rt) renderLoop(l *ForNode, model interface{}) {
	val, err := r.evalExpression(l.Expression, model)
	if err != nil {
		r.errors.Push(err)
		return
	}

	its, err := iteratorFor(val)
	if err != nil {
		r.errors.Push(err)
		return
	}

	for i, e := range its {

		r.pushVar(l.Value, e.value)
		if l.Key != "" {
			r.pushVar(l.Key, e.key)
		}

		if l.First {
			r.pushVar("first", i == 0)
		}
		if l.Last {
			r.pushVar("last", i == len(its)-1)
		}
		if l.Even {
			r.pushVar("even", i%2 == 0)
		}

		for _, n := range l.Nodes {
			r.renderNode(n, model)
		}

		r.popVar()
		if l.Key != "" {
			r.popVar()
		}
		if l.First {
			r.popVar()
		}
		if l.Last {
			r.popVar()
		}
		if l.Even {
			r.popVar()
		}
	}
}

func (r *rt) renderNode(n Node, model interface{}) {
	switch node := n.(type) {
	case *TextNode:
		r.append(node.Text)
	case *EchoNode:
		r.renderEcho(node, model)
	case *VisitNode:
		r.renderVisit(node, model)
	case *ForNode:
		r.renderLoop(node, model)
	case *IfNode:
		r.renderIf(node, model)
	case *IndentNode:
		r.renderIndent(node, model)
	default:
		panic(fmt.Errorf("unknown node %T", n))
	}
}

func (r *rt) render(model interface{}) error {
	tpl, err := r.getTemplate(model)

	if err != nil {
		return err
	}

	for _, n := range tpl.Nodes {
		r.renderNode(n, model)
	}
	return nil
}

func (r *rt) getTemplate(model interface{}) (*Template, error) {
	visitor, err := r.getVisitorInstance()
	if err != nil {
		return nil, err
	}

	tp := fmt.Sprintf("%T", model)
	// normalize type by striping pointers
	tp = strings.ReplaceAll(tp, "*", "")

	for _, t := range visitor.Templates {
		if t.Name == tp {
			return t, nil
		}
	}

	// no exact type found, check for default
	for _, t := range visitor.Templates {
		if t.Name == "*" {
			return t, nil
		}
	}

	// no default found, error
	return nil, fmt.Errorf("could not find template for type '%v' for visitor '%v'", tp, r.getVisitor())
}

func (r *rt) getVisitorInstance() (*Visitor, error) {
	for _, v := range r.ast.Visitors {
		if v.Name == r.getVisitor() {
			return v, nil
		}
	}
	return nil, fmt.Errorf("no visitor '%v' defined", r.getVisitor())
}
