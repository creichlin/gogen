package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/gogen/parser"
)

type result struct {
	Errors []errMsg   `json:"errors"`
	Ast    *gogen.AST `json:"ast"`
}

type errMsg struct {
	Message string `json:"message"`
	Line    int    `json:"line"`
	Column  int    `json:"column"`
}

func main() {
	dat, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	ast, errs := parser.ParseDetailed(string(dat), "", func(i string) (string, string, error) {
		return "", "", nil
	})

	r := result{
		Ast: ast,
	}
	for _, e := range errs.Get() {
		ge, is := e.(*gogen.Error)
		if is {
			_, l, c := ge.Location()
			r.Errors = append(r.Errors, errMsg{
				Message: ge.Error(),
				Line:    l,
				Column:  c,
			})
		} else {
			r.Errors = append(r.Errors, errMsg{
				Message: e.Error(),
				Line:    0,
				Column:  0,
			})
		}
	}

	d, err := json.MarshalIndent(&r, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(d))
}
