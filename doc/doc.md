GoGen Documentation
===================


Basics
------

### Hello world

Each template must have one or more visitors and for each visitor there are one or more types. The simplest template must have a `main` visitor and the type must be the root element of the model that gets rendered.

Template:

    main visitor:
    test.Empty type:
      Hello ${"World"}
    

Output:

    Hello World

### Print statement

The print syntax `${...}` can be used to print expressions like literals or results of operations on literals.

Template:

    main visitor:
    test.Empty type:
      ${1+2} ${true} ${0.010 / 10} ${"mi" * 3}
    

Output:

    3 true 0.001 mimimi

### Model

Whenever a type is rendered it's instance is accessible by the `.` expression. For the type `test.Basic` the expression `.Title` will return the `Title` attribute of the rendered Instance.

Template:

    main visitor:
    test.Basic type:
      Title: ${.Title}
      ${.Meta.Name} (${.Meta.Count})
    

Output:

    Title: TITLE
    NAME (3)

### Visit child type

The visit syntax `@{...}` will render the template for the given parameter.

Template:

    main visitor:
    test.Basic type:
      <@{.Meta}>
    test.Meta type:
      :${.Name}:
    

Output:

    <:NAME:>


Comments
--------

### Outside visitors

Comments can only be placed outside of visitors and must start with `//` and no leading spaces.

Template:

    // comment for main visitor
    main visitor:
    
    // comment for a first type
    test.Empty type:
      @{"foo"}/@{2} @{detail: "foo"}/@{detail: 2}
    
    // A comment
    string type:
      A
    
    // B comment
    int64 type:
      B
    
    
    // comment for another visitor
    
    detail visitor:
    
    // somewhere inside commen
    
    // comment for another type
    // with even two lines!
    string type:
      AA
    int64 type:
      BB
    // and the end comment
    

Output:

    A/B AA/BB


Data types
----------

### Bool

Bools are either `true` or `false`. Boolean operations like `&&` or '!' must have boolean values as operands. The 0 value will not evaluate to `false` (this will only work on the result of an expression inside an if statement).

Template:

    main visitor:
    test.Empty type:
      ${false} ${true}
    

Output:

    false true

### Int literals

Int literals are always signed int64. Operations on int values from the model which are not int64 will always be converted first to int64. When a minus value is declared one has to be carefull that there is a space betwwen a `{` otherwise it is interpreted as a `{-` and a positive number.

Template:

    main visitor:
    test.Empty type:
      ${100} ${ -10} ${0} ${12345}
    

Output:

    100 -10 0 12345

### string literals

Just strings

Template:

    main visitor:
    test.Empty type:
      ${"foo"} ${"bar"} ${"eagle with horns"}
    

Output:

    foo bar eagle with horns

### float literals

Floats are always float64. Float32 from the model will be converted to float64 when used in an operation. Operations with an int and a float will result in float64. When a minus value is declared one has to be carefull that there is a space betwwen a `{` otherwise it is interpreted as a `{-` and a positive number.

Template:

    main visitor:
    test.Empty type:
      ${0.45} ${ -12654.43543}
    

Output:

    0.45 -12654.43543


Expressions
-----------

### boolean expressions

The boolean operations have the common precedence !, ==/!=, &&, ||.

Template:

    main visitor:
    test.Empty type:
      ${true || false && false} because && has higher precedence
      ${(true || false) && false} with changed precedence
      ${!true} is false
      ${true == false} is false
      ${true != false} is true
    

Output:

    true because && has higher precedence
    false with changed precedence
    false is false
    false is false
    true is true

### comparison

Equality is defined on all primitive data types, less/greater comparisons are only defined on numeric types.

Template:

    main visitor:
    test.Empty type:
      ${7 < 8} 7 < 8
      ${2.0 < 1.9} 2.0 < 1.9
      ${"foo" == "bar"} ${"foo" != "bar"}
    

Output:

    true 7 < 8
    false 2.0 < 1.9
    false true

### strings

Strings can be concatenated with any type but the first parameter must be of type string. A string can be multiplied with an int which results in n repetitions of the string.

Template:

    main visitor:
    test.Empty type:
      ${"foo" + true} ${"bar" + 7}
      ${"=" * 12}
    

Output:

    footrue bar7
    ============

### Maths

Mathematical operations with float and int mixed will always result in float values.

Template:

    main visitor:
    test.Empty type:
      2 * 5 = ${2 * 5}
      15 / 4 = ${15/4}
      15 / 4.0 = ${15/4.0}
      ${15 - 13.0 | type}
    

Output:

    2 * 5 = 10
    15 / 4 = 3
    15 / 4.0 = 3.75
    float64


If statement
------------

### if

The block inside an if is only rendered if the expression evaluates to true.

Template:

    main visitor:
    test.Empty type:
      {if true}true{end}{if false}false{end}
    

Output:

    true

### if else

If an else is appended the block after the if or after the else is rendered depending on the expression.

Template:

    main visitor:
    test.Empty type:
      {if true}true{else}false{end}
      {if false}false{else}true{end}
    

Output:

    true
    true

### If on non boolean values

If the expression avaluates to a non boolean value it will still work. It will behave as if the value is true except for empty, 0 or null values. This depends on the type.

This works only inside the if statement. Expressions that work on booleans do not work on non boolean values. So `!2` will casue an error and not evaluate to `false`.


Template:

    main visitor:
    test.Empty type:
      "foo": {if "foo"}true{else}false{end}
      "":    {if ""}true{else}false{end}
      2:     {if 2}true{else}false{end}
      0:     {if 0}true{else}false{end}
      2.9:   {if 2.9}true{else}false{end}
      0.0:   {if 0.0}true{else}false{end}
    

Output:

    "foo": true
    "":    false
    2:     true
    0:     false
    2.9:   true
    0.0:   false


Whitespace
----------

### Around statements

Whitespace can be ommited before and after a statement. To do so one must insert a - before the opening or closing brace and the expression itself.

Template:

    main visitor:
    test.Empty type:
      A
      {-if true} B {end-}
      {if true-} C {-end}
      {for x in "123"-}
      ${x}
      {-end}
    

Output:

    A B C
    123

### Around echo

An echo statement can have - to omit whitespace as well.

Template:

    main visitor:
    test.Empty type:
      func(
        ${-"foo"-}
        , ${"bar"-}
      )
    

Output:

    func(foo, bar)

### Around visit

A visit statement can have - to omit whitespace as well.

Template:

    main visitor:
    test.Empty type:
      func(
        @{-"foo"-}
        , @{"bar"-}
      )
    string type:
      "${.}"
    

Output:

    func("foo", "bar")


Filters
-------

### Basics

Values can be piped trough filters using the `|` symbol. Filters are single parameter functions which take the input from the left, and output a modified copy of it.

Template:

    main visitor:
    test.Empty type:
      ${"foo" | upper}
      ${"fOO" | lower}
    

Output:

    FOO
    foo

### Chaining

Multiple filters in a row are executed from left to right.

Template:

    main visitor:
    test.Empty type:
      ${"foO" | upper | lower}
    

Output:

    foo

### len filter

The len filter returns the length of the given slice, array, slice or map. If len is applied on a nil slice or map it will return 0.

Template:

    main visitor:
    test.Filters type:
      ${.String | len}
      ${.Array | len}
      ${.Slice | len}
      ${.Map | len}
      ${.NilSlice | len}
      ${.NilMap | len}
    

Output:

    3
    3
    3
    3
    0
    0

### type filter

The type filter returns the go-syntax representation of the type of the value.

Template:

    main visitor:
    test.Filters type:
      ${. | type}
      ${.String | type}
      ${.Array | type}
      ${.Slice | type}
      ${.Map | type}
      ${.Items | type}
    

Output:

    *test.Filters
    string
    [3]string
    []string
    map[string]int
    []*test.FilterItem

