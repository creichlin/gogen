const updateTOC = _.throttle(() => {
  const top = $(document).scrollTop();
  let current = "c0";
  $(".main > .content > h2").each((i, t) => {
    const topOffset = $(t).offset().top;
    if (top > topOffset - 10) {
      current = "c" + i;
    }
  });

  const currentElement = $("#nav-" + current);
  if (currentElement.hasClass("active")) {
    return;
  }

  $(".nav1").removeClass("active");
  currentElement.addClass("active");
  window.history.replaceState(current, current, "#" + current);
}, 200);

$.when($.ready).then(() => {
  $(window).scroll(updateTOC);
  updateTOC();
});
