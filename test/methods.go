package test

type Methods struct {
	Slice SliceWithMethod
}

func (m *Methods) NoParamStrOnPtr() string {
	return "no-param-str-on-ptr"
}

func (m Methods) NoParamStr() string {
	return "no-param-str"
}

type SliceWithMethod []string

func (swm SliceWithMethod) NoParamStr() string {
	return "slice-no-param-str"
}

var methods = &Methods{}
