model: Expressions
order: 5
name: Expressions
tests:
  - name: boolean expressions
    description: The boolean operations have the common precedence !, ==/!=, &&, ||.
    model: Empty
    template: |
      main visitor:
      test.Empty type:
        ${true || false && false} because && has higher precedence
        ${(true || false) && false} with changed precedence
        ${!true} is false
        ${true == false} is false
        ${true != false} is true
    expected: |-
      true because && has higher precedence
      false with changed precedence
      false is false
      false is false
      true is true

  - name: comparison
    description: Equality is defined on all primitive data types, less/greater comparisons are only defined on numeric types.
    model: Empty
    template: |
      main visitor:
      test.Empty type:
        ${7 < 8} 7 < 8
        ${2.0 < 1.9} 2.0 < 1.9
        ${"foo" == "bar"} ${"foo" != "bar"}
    expected: |-
      true 7 < 8
      false 2.0 < 1.9
      false true

  - name: strings
    description: Strings can be concatenated with any type but the first parameter must be of type string. A string can be multiplied with an int which results in n repetitions of the string.
    model: Empty
    template: |
      main visitor:
      test.Empty type:
        ${"foo" + true} ${"bar" + 7}
        ${"=" * 12}
    expected: |-
      footrue bar7
      ============

  - name: Maths
    description: Mathematical operations with float and int mixed will always result in float values.
    model: Empty
    template: |
      main visitor:
      test.Empty type:
        2 * 5 = ${2 * 5}
        15 / 4 = ${15/4}
        15 / 4.0 = ${15/4.0}
        ${15 - 13.0 | type}
    expected: |-
      2 * 5 = 10
      15 / 4 = 3
      15 / 4.0 = 3.75
      float64

  - name: not expression
    template: |
      main visitor:
      test.Expressions type:
        ${!false} ${!!true} ${!!!true}
    expected: true true false

  - name: int operands orders/types
    template: |
      main visitor:
      test.Expressions type:
        ${5 < .I10} ${15 < .I10}
        ${ .I10 < 15} ${ .I10 < 5}
        ${5 < 10} ${5 > 10}
        ${.I0 < .I10} ${.I10 > .I0}
    expected: |-
      true false
      true false
      true false
      true true

  - name: int operands
    template: |
      main visitor:
      test.Expressions type:
        ${5 < 10} ${5 > 10}
        ${5 <= 5} ${5 >= 5} ${5 >= 6} ${6 <= 5}
        ${5 == 5} ${5 != 5} ${3 == 6} ${3 != 6}
    expected: |-
      true false
      true true false false
      true false false true

  - name: int arithmetic
    template: |
      main visitor:
      test.Expressions type:
        2 * 5 = ${2 * 5}
        2 + 5 = ${2 + 5}
        15 / 4 = ${15/4}
        17 % 6 = ${17 % 6}
        15 - 13 = ${15 - 13}
    expected: |-
      2 * 5 = 10
      2 + 5 = 7
      15 / 4 = 3
      17 % 6 = 5
      15 - 13 = 2
  - name: float arithmetic
    template: |
      main visitor:
      test.Expressions type:
        2.0 * 5 = ${2.0 * 5}
        2 + 5.0 = ${2 + 5.0}
        15 / 4.0 = ${15/4.0}
        15.0 - 13.0 = ${15.0 - 13.0}
    expected: |-
      2.0 * 5 = 10
      2 + 5.0 = 7
      15 / 4.0 = 3.75
      15.0 - 13.0 = 2

  - name: string concatenation
    template: |
      main visitor:
      test.Expressions type:
        ${"foo" + "bar" | upper}
    expected: FOOBAR

  - name: slice concatenation
    template: |
      main visitor:
      test.Expressions type:
        ${.IntSlice + .StrSlice} ${(.IntSlice + .StrSlice) | type}
        ${.IntSlice + .IntSlice} ${(.IntSlice + .IntSlice) | type}
        ${.StrSlice + .StrSlice} ${(.StrSlice + .StrSlice) | type}

    expected: |-
      [4 7 foo bar] []interface {}
      [4 7 4 7] []int
      [foo bar foo bar] []string
