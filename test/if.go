package test

type If struct {
	True  bool
	False bool

	NilString    *string
	NilStruct    *IfStruct
	NilInterface IfInterface
}

type IfInterface interface {
	Foo()
}

type IfStruct struct {
	Foo bool
}

var ifModel = &If{
	True: true,
}
