package test

type Import struct {
	Name  string
	Count int
}

var import_ = &Import{
	Name:  "barbel",
	Count: 2,
}
