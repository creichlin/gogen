package test

type Indent struct {
	Multiline string
	Number    int
}

var indent = &Indent{
	Multiline: `First line
Second Line
Third Line`,
	Number: 5,
}
