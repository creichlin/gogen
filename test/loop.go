package test

type Loop struct {
	Slice  []Item
	SliceP []*Item
	PSlice *[]Item
	Map    map[string]Item
	String string
}

type Item struct {
	Name string
}

var loop = &Loop{
	Slice:  []Item{{Name: "A"}, {Name: "B"}, {Name: "C"}},
	Map:    map[string]Item{"a": {Name: "A"}, "b": {Name: "B"}, "c": {Name: "C"}},
	String: "XYZ€",
}
